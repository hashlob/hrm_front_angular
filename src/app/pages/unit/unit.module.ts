import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnitRoutingModule } from './unit-routing.module';
import { AddUnitComponent } from './add-unit/add-unit.component';
import { ListUnitComponent } from './list-unit/list-unit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [AddUnitComponent, ListUnitComponent],
  imports: [
    CommonModule,
    UnitRoutingModule,
    FormsModule,
    NgSelectModule,
    NgbDropdownModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ]
})
export class UnitModule { }
