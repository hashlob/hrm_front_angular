import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  baseUrl = AppConfig.API_ENDPOINT + "/unit";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  addUnit(payload) {
    return this.http.post(this.baseUrl, payload, {
      headers: this.headers
    }).toPromise()
  }

  getAllUnits() {
    return this.http.get(this.baseUrl, {
      headers: this.headers
    }).toPromise()
  }

  deleteUnit(payload) {
   
    return this.http.post(`${this.baseUrl}/delete`, payload , {
      headers: this.headers
    }).toPromise()
  }

  getUnitById(id) {
    return this.http.get(`${this.baseUrl}/getUnitById/${id}`, {
      headers: this.headers
    }).toPromise()
  }
}
