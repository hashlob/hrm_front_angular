import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddUnitComponent } from './add-unit/add-unit.component';
import { ListUnitComponent } from './list-unit/list-unit.component';

const routes: Routes = [{
  path: "",
  children: [
    {
      path: "list",
      component: ListUnitComponent,
      data: {
        title: "Add Unit"
      }
    },
    {
      path: "add",
      component: AddUnitComponent,
      data: {
        title: "Edit Unit"
      }
    },
    {
      path: "add/:id",
      component: AddUnitComponent,
      data: {
        title: "Edit Unit"
      }
    },
  ]

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnitRoutingModule { }
