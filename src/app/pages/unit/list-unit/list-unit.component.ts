import { Component, OnInit } from '@angular/core';
import { UnitService } from '../unit.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-unit',
  templateUrl: './list-unit.component.html',
  styleUrls: ['./list-unit.component.scss']
})
export class ListUnitComponent implements OnInit {



  rows = [];
  columns = [
    { name: 'No' },
    { name: 'Name' },
    { name: 'Email' },
    { name: 'Phone' },
    { name: 'Prefix' },
    { name: 'Type' },
    { name: 'Start Time' },
    { name: 'End Time' },
    { name: 'Week Start' },
    { name: 'Address' },
    { name: 'Actions' }

  ];

  loadingState = true
  constructor(
    private unitService: UnitService,
    private toaster: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.unitService.getAllUnits().then((res: any) => {
      if (res.code == 200) {
        this.loadingState = false
        this.rows = res.data
      }
    })
  }

  deleteUnit(id) {
    let formData = new FormData;
    formData.append('id', id);
    this.unitService.deleteUnit(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          let row = this.rows.filter(x => x.id != id)
          this.rows = row
          this.toaster.success(message)
        }
      }).catch(err => {
        console.log(err)
      })
  }

}
