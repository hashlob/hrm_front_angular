import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UnitService } from '../unit.service';
import { SharedService } from 'app/shared/shared.service';
import AppConfig from 'app/shared/app-config/appConfig';
import { ZoneService } from 'app/pages/zone/zone.service';

@Component({
  selector: 'app-add-unit',
  templateUrl: './add-unit.component.html',
  styleUrls: ['./add-unit.component.scss']
})
export class AddUnitComponent implements OnInit {

  constructor(
    private unitService: UnitService,
    private fb: FormBuilder,
    private router: Router,
    private toaster: ToastrService,
    private sharedService: SharedService,
    private zoneService: ZoneService,
    private activeRoutes: ActivatedRoute

  ) { }

  unitForm = this.fb.group({
    name: ['', [Validators.required]],
    email: [],
    phone: [],
    code: [],
    prefix: [],
    shift_start: [],
    shift_end: [],
    week_start: [],
    type: [],
    address: [],
    zone_id: [],
  })
  unitTypes = []
  zones = []
  unitDetail
  id

  formErrorStatus: boolean = false
  formError = ""
  ngOnInit() {
    this.id = this.activeRoutes.snapshot.params.id
    if (this.id) {
      this.fetchUnitDetail()
    }
    this.fetchUnitTypes()
    this.fetchZones()
  }

  fetchUnitDetail() {
    this.unitService.getUnitById(this.id).
      then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.unitDetail = data
          this.setForm()
        }
      }).catch(err => {
        console.log(err)
      })
  }

  setForm() {
    this.unitForm.get('name').setValue(this.unitDetail.name)
    this.unitForm.get('email').setValue(this.unitDetail.email)
    this.unitForm.get('code').setValue(this.unitDetail.code)
    this.unitForm.get('phone').setValue(this.unitDetail.phone)
    this.unitForm.get('prefix').setValue(this.unitDetail.prefix)
    this.unitForm.get('type').setValue(this.unitDetail.type)
    this.unitForm.get('zone_id').setValue(this.unitDetail.zone_id)
    this.unitForm.get('shift_start').setValue(this.unitDetail.shift_start)
    this.unitForm.get('shift_end').setValue(this.unitDetail.shift_end)
    this.unitForm.get('week_start').setValue(this.unitDetail.week_start)
    this.unitForm.get('address').setValue(this.unitDetail.address)
  }

  fetchZones() {
    this.zoneService.getZones({})
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.zones = data
        }
      }).catch(err => {
        console.log(err)
      })
  }

  fetchUnitTypes() {
    this.sharedService.getDBVariable({ type_id: AppConfig.DBVariablesId.Unit })
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.unitTypes = data[0].details
        }

      }).catch(err => {
        console.log(err)
      })
  }

  addUnit() {
    let formData = new FormData();
    formData.append("name", this.unitForm.value.name)
    formData.append("email", this.unitForm.value.email)
    formData.append("phone", this.unitForm.value.phone)
    formData.append("code", this.unitForm.value.code)
    formData.append("prefix", this.unitForm.value.prefix)
    formData.append("shift_start", this.unitForm.value.shift_start)
    formData.append("shift_end", this.unitForm.value.shift_end)
    formData.append("week_start", this.unitForm.value.week_start)
    formData.append("type", this.unitForm.value.type)
    formData.append("address", this.unitForm.value.address)
    formData.append("zone_id", this.unitForm.value.zone_id)
    if (this.id) {
      formData.append('id', this.id)
    }
    this.unitService.addUnit(formData).then((res: any) => {
      if (res.code == 200) {
        this.toaster.success("Unit Created");
        this.router.navigate(['/unit/list'])
      }
    })
  }
}
