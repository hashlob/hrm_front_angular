import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestTransferComponent } from './request-transfer/request-transfer.component';
import { RequestsListComponent } from './requests-list/requests-list.component';

const routes: Routes = [{
  path: "",
  children: [{
    path: 'requesttransfer',
    component: RequestTransferComponent,
    data: {
      title: 'Request Transfer'
    },
  },
{
  path : "requestslist",
  component:RequestsListComponent,
  data : {
    title : "Requests List"
  }
}
]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransferManagementRoutingModule { }
