import { TestBed } from '@angular/core/testing';

import { TransferManagementService } from './transfer-management.service';

describe('TransferManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TransferManagementService = TestBed.get(TransferManagementService);
    expect(service).toBeTruthy();
  });
});
