import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransferManagementRoutingModule } from './transfer-management-routing.module';
import { RequestTransferComponent } from './request-transfer/request-transfer.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RequestsListComponent } from './requests-list/requests-list.component';
import { NgbTabsetModule, NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [RequestTransferComponent, RequestsListComponent],
  imports: [
    CommonModule,
    NgbTabsetModule,
    NgbDropdownModule,
    NgbModalModule,
    NgxDatatableModule,
    TransferManagementRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TransferManagementModule { }
