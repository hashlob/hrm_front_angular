import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SharedService } from 'app/shared/shared.service';
import * as moment from 'moment';
import AppConfig from 'app/shared/app-config/appConfig';
import { TransferManagementService } from '../transfer-management.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-request-transfer',
  templateUrl: './request-transfer.component.html',
  styleUrls: ['./request-transfer.component.scss']
})
export class RequestTransferComponent implements OnInit {
  dropDownEmployee: any = []
  dropDownTransfer: any = []
  currentDate
  constructor(
    private fb: FormBuilder,
    private shared: SharedService,
    private requestService: TransferManagementService,
    private toaster: ToastrService) { }



  requestGenerateForm = this.fb.group(
    {
      to: [],
      request_date: moment(this.currentDate).format('YYYY-MM-DD'),
      reason: [],
      user_id: [],
      initiated_by: AppConfig.getUserId(),
      // department_id: AppConfig.getDepartmentId()

    }
  )
  ngOnInit() {
    this.currentDate = new Date()

    this.fetchEmployee()
    this.fetchtransfer()
  }
  requestTranfer = this.fb.group({

  })

  fetchEmployee() {
    this.shared.getUsersForSelect({ transferRequest: true }).then((res: any) => {
      this.dropDownEmployee = res.data.map((i) => {
        return {
          name: i.user.first_name + " " + i.user.last_name,
          id: i.user.id
        }
      })
    })
  }

  fetchtransfer() {
    this.shared.getUnitForSelect({}).then((res: any) => {
      this.dropDownTransfer = res.data.map((i) => {
        return {
          name: i.name,
          id: i.id
        }
      })
    })
  }

  ganerateRequest() {
    this.requestService.generateRequest(this.requestGenerateForm.value).then((res: any) => {
      let { code, message, data } = res

      if (code == 200) {
        this.toaster.success(message)
      }
    }).catch(err => {
      let { status, error } = err
      if (status == 422) {
        this.toaster.error(error.message)
      }

    })
  }
}
