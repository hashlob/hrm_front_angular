import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TransferManagementService {
  baseUrl = AppConfig.API_ENDPOINT + "/transferRequests";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
   }

   generateRequest(payload){
     return this.http.post(`${this.baseUrl}/generateRequest`,payload,{
       headers : this.headers
     }).toPromise()

   }
d
   fetchRequestsList(){
     return this.http.get(`${this.baseUrl}/getTransferRequests`,{
       headers : this.headers
     }).toPromise()
   }

   acceptReject(payload){
     return this.http.post(`${this.baseUrl}/processTransferRequest`,payload,{
      headers : this.headers

     }).toPromise()
   }
}
