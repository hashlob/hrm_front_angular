import { Component, OnInit } from '@angular/core';
import { TransferManagementService } from '../transfer-management.service';
import AppConfig from 'app/shared/app-config/appConfig';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.scss']
})
export class RequestsListComponent implements OnInit {
  rows = [];
  _id
  transferid
  constructor(private service: TransferManagementService, private toaster: ToastrService, private modalService: NgbModal, private fb: FormBuilder,
  ) { }
  columns = [
    { name: 'No' },
    { name: 'Employee' },
    { name: 'Request Date' },

    { name: 'To' },
    { name: 'From' },
    {name : "Status"},
    { name: 'Actions' },
  ];
  rejectForm = this.fb.group({
    transfer_id: [],
    status: [],
    manager_id: AppConfig.getUserId(),
    reason: [],
    department_id: AppConfig.getDepartmentId()


  })
  // rejectForm = this.fb.group({

  // })
  ngOnInit() {
    this._id = AppConfig.getUserId()

    this.fetchRequestList()
  }





  fetchRequestList() {

    this.service.fetchRequestsList().then((res: any) => {
      debugger
      this.rows = res.data


    }).catch(err => {

    })
  }




  open(content, id) {
    this.transferid = id
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }
  saveReject() {
    this.rejectForm.patchValue({
      transfer_id: this.transferid,
      status: 0
    })
    this.service.acceptReject(this.rejectForm.value).then((res: any) => {

      this.close()
      this.toaster.success(res.message)
      this.fetchRequestList()

    }).catch(err => {
      this.toaster.error(err.message)
    })
  }
  accept(id) {
    this.rejectForm.patchValue({
      transfer_id: id,
      status: 1,
      reason: null
    })
    this.service.acceptReject(this.rejectForm.value).then((res: any) => {
      this.toaster.success(res.message)
      this.fetchRequestList()
    }).catch(err => {

    })
  }
}
