import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PermissionRoutingModule } from './permission-routing.module';
import { PermissionComponent } from './permission/permission.component';
import { NgbTabsetModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';
import { ListComponent } from './permission/list/list.component';
import { AssignPermissionComponent } from './permission/assign-permission/assign-permission.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PermissionComponent, ListComponent, AssignPermissionComponent],
  imports: [
    CommonModule,
    PermissionRoutingModule,
    NgbTabsetModule,
    NgbDropdownModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ]
})
export class PermissionModule { }
