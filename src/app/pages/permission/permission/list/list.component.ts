import { Component, OnInit } from '@angular/core';
import { PermissionService } from '../../permission.service';
import { FormBuilder, Validators, NgModel } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  permissionDetails = []
  columns = [
    { name: 'No' },
    { name: 'Name' },
    { name: 'Status' },
    { name: 'Actions' }
  ];
  permissionStatus = [
    { name: "Active", status: "1" },
    { name: "Inactive", status: "0" }
  ]
  constructor(
    private permissionService: PermissionService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private toaster: ToastrService
  ) { }

  permissionForm = this.fb.group({
    name: ['', [Validators.required]],
    is_active: ['', [Validators.required]],
    id: []
  })
  id = null
  permissionDetail = null

  ngOnInit() {
    this.getAllPermissions()
  }
  open(content, id = null) {
    this.permissionForm.reset()
    if (id) {
      this.id = id
      this.getPermissionById(id)
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  getPermissionById(id) {
    this.permissionService.getPermissionById(id)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.permissionDetail = data
          this.setForm()
        }
      }).catch(err => console.log(err))
  }

  setForm() {
    this.permissionForm.get("is_active").setValue(this.permissionDetail.is_active)
    this.permissionForm.get("name").setValue(this.permissionDetail.name)
    this.permissionForm.get("id").setValue(this.permissionDetail.id)
  }

  getAllPermissions() {
    this.permissionService.getAllPermissions({})
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.permissionDetails = data
        }
      }).catch(err => console.log(err))
  }

  addPermission() {

    this.permissionService.addPermissions(this.permissionForm.value)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.permissionForm.reset()
          this.toaster.success(message)
          this.getAllPermissions()
          this.id = null
          this.close()
        }
      }).catch(err => console.log(err))
  }

}
