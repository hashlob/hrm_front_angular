import { Component, OnInit } from '@angular/core';
import { PermissionService } from '../../permission.service';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from 'app/shared/shared.service';

@Component({
  selector: 'app-assign-permission',
  templateUrl: './assign-permission.component.html',
  styleUrls: ['./assign-permission.component.scss']
})
export class AssignPermissionComponent implements OnInit {

  rolePermissionDetails = []
  roles = []
  permissions = []
  columns = [
    { name: 'No' },
    { name: 'Assigned Permission' },
    { name: 'Actions' }
  ];
  loading = true
  role_id = null
  constructor(
    private permissionService: PermissionService,
    private sharedService: SharedService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private toaster: ToastrService
  ) { }
  permissionForm = this.fb.group({
    permission_id: ['', [Validators.required]],
    role_id: ['', [Validators.required]],
  })
  ngOnInit() {
    this.fetchRoles()
    this.fetchAllPermissions()
  }

  fetchRoles() {
    let params = {
      all: true
    }
    this.sharedService.getRolesForSelect(params)
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.roles = data
        }
      }).catch(err => console.log(err))
  }

  fetchAllPermissions() {
    this.permissionService.getAllPermissions({})
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.permissions = data
        }
      }).catch(err => console.log(err))
  }

  onRoleChange(event) {
    this.role_id = event.id
    this.fetchRolePermissionsByRole(event.id)
  }

  fetchRolePermissionsByRole(id) {
    let params = {
      role_id: id
    }

    this.permissionService.getPermissionByRole(params)
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.rolePermissionDetails = data[0].permissions
        }
      }).catch(err => console.log(err))
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  addRolePermission() {
    this.permissionService.addRolePermission(this.permissionForm.value)
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.permissionForm.reset()
          this.close()
          this.fetchRolePermissionsByRole(this.role_id)
          this.toaster.success(message)
          this.role_id = null
        }
      }).catch(err => console.log(err))
  }

  removePermission(id) {
    let payload = {
      role_id: this.role_id,
      permission_id: id
    }
    this.permissionService.removePermission(payload)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.toaster.success(message)
          let filtered = this.rolePermissionDetails.filter(x => x.id != id)
          this.rolePermissionDetails = filtered
        }
      }).catch(err => console.log(err))
  }

}
