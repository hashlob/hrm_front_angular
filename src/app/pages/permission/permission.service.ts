import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  baseUrl = AppConfig.API_ENDPOINT + "/permission";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  getAllPermissions(params) {
    return this.http.get(`${this.baseUrl}`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  addPermissions(payload) {
    return this.http.post(`${this.baseUrl}`, payload, {
      headers: this.headers
    }).toPromise()
  }

  getPermissionById(id) {
    return this.http.get(`${this.baseUrl}/getPermissionById/${id}`, {
      headers: this.headers
    }).toPromise()
  }

  getPermissionByRole(params) {
    return this.http.get(`${this.baseUrl}/rolePermission/getPermissionByRole`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  addRolePermission(payload) {
    return this.http.post(`${this.baseUrl}/rolePermission`, payload, {
      headers: this.headers
    }).toPromise()
  }

  removePermission(payload) {
    return this.http.post(`${this.baseUrl}/rolePermission/delete`, payload, {
      headers: this.headers
    }).toPromise()
  }
}
