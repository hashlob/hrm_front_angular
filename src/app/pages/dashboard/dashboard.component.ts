import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from "ng-chartist";
import * as moment from "moment"

export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  displayCardData = {
    employees: 0
  }
  attendanceCardData = {
    totalEmployees: 0,
    totalPresent: 0,
    totalOnLeave: 0,
    totalAbsent: 0,
    notMarked: 0
  }
  weeksArray = []

  attendanceBarChart: Chart = {
    type: 'Bar',
    data: {},
    options: {
      axisX: {
        showGrid: false,
        showLabel: false,
        offset: 0
      },
      axisY: {
        showGrid: false,
        showLabel: false,
        offset: 0
      },
      low: 0,
    },
    responsiveOptions: [
      ['screen and (max-width: 640px)', {
        seriesBarDistance: 5,
        axisX: {
          labelInterpolationFnc: function (value) {
            return value[0];
          }
        }
      }]
    ],
    events: {

      draw(data: any): void {
        var barHorizontalCenter, barVerticalCenter, label, value;
        if (data.type === 'bar') {

          data.element.attr({
            y1: 195,
            x1: data.x1 + 0.001
          });

        }
      }
    },
  }

  weeklyAttendanceBarChart: Chart = {
    type: "Bar",
    data: {},
    options: {
      axisX: {
          showGrid: false,
      },
      axisY: {
          low: 0,
          scaleMinSpace: 50,
      },
      // fullWidth: true
  },
    responsiveOptions: [
      ['screen and (max-width: 640px)', {
        seriesBarDistance: 5,
        axisX: {
          labelInterpolationFnc: function (value) {
            return value[0];
          }
        }
      }]
    ],
    events: {
      draw(data: any): void {
          if (data.type === 'label') {
              // adjust label position for rotation
              const dX = data.width / 2 + (30 - data.width)
              data.element.attr({ x: data.element.attr('x') - dX })
          }
      }
  },
  }

  constructor(
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    this.initRequests()
  }

  initRequests() {
    this.getDisplayCardsData()
    this.getAttendaceCardsData()
  }

  getDisplayCardsData() {
    let obj = {
      fromDate: moment().startOf("month").format("YYYY-MM-DD"),
      toDate: moment().endOf("month").format("YYYY-MM-DD")
    }
    this.dashboardService.getDisplayCardsData(obj)
      .then((res: any) => {
        this.displayCardData = res.data
      }).catch(err => console.log(err))
  }

  getAttendaceCardsData() {
    let fromDate = moment().startOf("month")
    let toDate = moment().endOf("month")
    let obj = {
      date: moment().format("YYYY-DD-MM"),
      fromDate: moment().startOf("month").format("YYYY-MM-DD"),
      toDate: moment().endOf("month").format("YYYY-MM-DD")
    }
    let noOfWeeks = moment().weeks() - moment().add(0, 'month').startOf('month').weeks() + 1


    this.dashboardService.getAttendaceCardsData(obj)
      .then((res: any) => {
        let { weeklyPresent, weeklyAbsent } = res.data
        this.attendanceCardData = res.data
        this.attendanceBarChart.data = {
          labels: [
            "Total",
            "Present",
            "On Leave",
            "Absent",
            "Not marked"
          ],
          series: [
            [
              this.attendanceCardData.totalEmployees,
              this.attendanceCardData.totalPresent,
              this.attendanceCardData.totalOnLeave,
              this.attendanceCardData.totalAbsent,
              this.attendanceCardData.notMarked,
            ],
          ]
        }

        //weekly attendance data
        let seriesArray = []
        for (let index = 0; index < noOfWeeks; index++) {
          this.weeksArray.push(`Week ${index + 1}`)
          let startWeek = fromDate.week()
          let p = weeklyPresent.filter(x => x.week == ((startWeek - 1) + index)).map(x => x.count).length == 0 ? 0 : weeklyPresent.filter(x => x.week == ((startWeek - 1) + index)).map(x => x.count).pop()
          let a = weeklyAbsent.filter(x => x.week == ((startWeek - 1) + index)).map(x => x.count).length == 0 ? 0 : weeklyAbsent.filter(x => x.week == ((startWeek - 1) + index)).map(x => x.count).pop()
          
          let row = [
            p,
            a,
            0,
            1,
          ]
          seriesArray.push(row)
        }
        console.log(JSON.stringify(seriesArray))
        this.weeklyAttendanceBarChart.data = {
         
          "labels": this.weeksArray,
          "series": [[0,1,0,1],[1,2,0,1],[0,0,0,1],[7,1,0,1],[0,0,0,1]]

        }
      }).catch(err => console.log(err))
  }

  convertNumber(number) {
    return Number(number).toLocaleString()
  }

  getCurrentMonth() {
    return moment().format("MMMM YYYY")
  }
}
