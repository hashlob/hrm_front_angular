import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  baseUrl = AppConfig.API_ENDPOINT + "/dashboard";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  getDisplayCardsData(params) {
    return this.http.get(`${this.baseUrl}/getDisplayCardsData`, {
      headers: this.headers,
      params
    }).toPromise();
  }

  getAttendaceCardsData(params) {
    return this.http.get(`${this.baseUrl}/getAttendaceCardsData`, {
      headers: this.headers,
      params: params
    }).toPromise();
  }
}
