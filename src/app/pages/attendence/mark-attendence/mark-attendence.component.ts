import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import * as moment from 'moment';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AttendanceService } from '../attendance.service';

@Component({
  selector: 'app-mark-attendence',
  templateUrl: './mark-attendence.component.html',
  styleUrls: ['./mark-attendence.component.scss'],
})
export class MarkAttendenceComponent implements OnInit {
  addTimeInOut = false
  dropDownEmployee: any = []
  statuss
  constructor(
    private share: SharedService,
    private fb: FormBuilder,
    private toaster: ToastrService,
    private attendanceService: AttendanceService
  ) { }

  markAttendence = this.fb.group({
    user_id: ['', [Validators.required]],
    date: ['', [Validators.required]],
    time_in: [],
    time_out: [],
    status: [],
    isPresent: [false]
  })

  ngOnInit() {
    let currentDate = new Date();
    this.fetchUsersForSelect(currentDate)
  }

  fetchUsersForSelect(date) {
    this.share.getUsersForSelect({ leaveSelect: true, date: moment(date).format('YYYY-MM-DD') }).then((res: any) => {
      this.dropDownEmployee = res.data.map(i => {
        return {
          name: i.first_name + " " + i.last_name,
          id: i.id
        }
      })
    })
  }



  isChecked(values) {
    this.addTimeInOut = values.currentTarget.checked ? true : false
    let value = values.currentTarget.checked ? 1 : 2
    this.markAttendence.get('status').setValue(value)

  }

  markAttendencebtn() {
    this.attendanceService.markAttendance(this.markAttendence.value)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.toaster.success(message)
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
        if (status == 422) {
          this.toaster.error(error.message)
        }

      })
  }

  dateChange(event) {
    let newdate = event.target.value
    this.fetchUsersForSelect(newdate)
  }

}

