import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as _  from 'lodash';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  baseUrl = AppConfig.API_ENDPOINT + "/attendance";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  markAttendance(payload) {
    return this.http.post(`${this.baseUrl}/markAttendance`, payload, {
      headers: this.headers
    }).toPromise()
  }

  getAttendanceReport(params) {
    return this.http.get(`${this.baseUrl}/getAttendanceReport`, {
      headers: this.headers,
      params
    }).toPromise()
  }
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    console.log('worksheet',worksheet);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
}
