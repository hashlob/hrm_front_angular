import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';

import { AttendenceRoutingModule } from './attendence-routing.module';
import { MarkAttendenceComponent } from './mark-attendence/mark-attendence.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AttendanceReportComponent } from './attendance-report/attendance-report.component';

@NgModule({
  declarations: [MarkAttendenceComponent, AttendanceReportComponent],
  imports: [
    CommonModule,
    AttendenceRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AttendenceModule { }
