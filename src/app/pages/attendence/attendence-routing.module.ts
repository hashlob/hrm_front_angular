import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarkAttendenceComponent } from './mark-attendence/mark-attendence.component';
import { AttendanceReportComponent } from './attendance-report/attendance-report.component';

const routes: Routes = [

  {
    path: '',
    children: [
      {
        path: 'markattendence',
        component: MarkAttendenceComponent,
        data: {
          title: 'Mark Attendence'
        }
      },
      {
        path: 'attendanceReport',
        component: AttendanceReportComponent,
        data: {
          title: 'Attendance Report'
        }
      },
    ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AttendenceRoutingModule { }
