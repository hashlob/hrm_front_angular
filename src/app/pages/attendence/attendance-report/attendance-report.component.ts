import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as moment from 'moment';
import { AttendanceService } from '../attendance.service';
import * as XLSX from "xlsx"
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { fontStyle } from 'html2canvas/dist/types/css/property-descriptors/font-style';
@Component({
  selector: 'app-attendance-report',
  templateUrl: './attendance-report.component.html',
  styleUrls: ['./attendance-report.component.scss']
})
export class AttendanceReportComponent implements OnInit {
  @ViewChild('TABLE') TABLE: ElementRef;
  daysInMonth = 0
  daysInMonthArray = []
  month = 0
  year = 0
  date = ''
  selectedMonth = ''
  attendanceArray = []
  dateStart = ''
  dateEnd = ''
  attendanceDetail = {}
  constructor(
    private attendanceService: AttendanceService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    let date = new Date;
    this.setMonth(date);
    this.fetchAttendanceReport()
  }

  setMonth(date) {
    this.month = moment(date).month()
    this.year = moment(date).year()
    this.date = moment(date).format("YYYY-MM-DD")
    this.dateStart = moment(date).startOf('month').format("YYYY-MM-DD")
    this.dateEnd = moment(date).endOf('month').format("YYYY-MM-DD")
    this.selectedMonth = moment(date).format("YYYY-MM")
    this.daysInMonth = moment(date).daysInMonth();
    this.daysInMonthArray = Array(this.daysInMonth).fill(this.daysInMonth).map((x, i) => i + 1)
    this.month = this.month + 1
    this.fetchAttendanceReport()
  }

  monthChange(e) {
    this.setMonth(e.target.value)
    this.fetchAttendanceReport()
  }

  fetchAttendanceReport() {

    let obj = {
      month: this.month,
      date: this.date,
      year: this.year,
      dateStart: this.dateStart,
      dateEnd: this.dateEnd,
    }
    this.attendanceService.getAttendanceReport(obj)
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.attendanceArray = data
          console.log(data)
        }
      }).catch(err => {

        console.log(err)
      })
  }

  // exportToExcel() {

  //   let report = [{
  //     eid: 'e101',
  //     ename: 'ravi',
  //     esal: 1000
  //   }, {
  //     eid: 'e102',
  //     ename: 'ram',
  //     esal: 2000
  //   }, {
  //     eid: 'e103',
  //     ename: 'rajesh',
  //     esal: 3000
  //   }]

  //   const ws: xlsx.WorkSheet = xlsx.utils.json_to_sheet(report);
  //   const wb: xlsx.WorkBook = xlsx.utils.book_new();
  //   xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
  //   xlsx.writeFile(wb, 'ScoreSheet.xlsx');
  // }

  exportToExcel() {
    let workbook = new Workbook();

    let worksheet = workbook.addWorksheet();
    let totalDaysDouble = this.daysInMonth * 2;

    const title = 'EMPLOYEE ATTENDANCE SHEET';
    const header = ["Employee Name", "Employee Code", "Unit", "Department", "Role"]
    worksheet.mergeCells(1, 1, 2, totalDaysDouble);
    worksheet.getCell(1, 1).font = { family: 4, size: 22, bold: true, color: { argb: "FF5E5E5E" } };
    worksheet.getCell(1, 1).value = title

    worksheet.getRow(3).values = [moment(this.selectedMonth).format('MMMM-YYYY')]
    worksheet.getRow(3).height = 25
    worksheet.getCell(3, 1).font = {
      color: { argb: "FF316886" },
      size: 18
    }

    worksheet.addRow([])
    worksheet.addRow([])

    //Add Header Row
    // Cell Style : Fill and Border
    let headerRow = worksheet.addRow(header);
    headerRow.eachCell((cell, number) => {
      cell.font = {
        bold: true,
      }
      cell.alignment = {
        vertical: "middle",
        horizontal: "center"

      }
    });
    worksheet.getColumn(1).width = 25
    worksheet.getColumn(2).width = 20
    worksheet.getColumn(3).width = 20
    worksheet.getColumn(4).width = 20
    worksheet.getColumn(5).width = 20

    /* Days and Dates - Iteration through columns */
    let index = 0
    while (totalDaysDouble > index) {
      // for days
      let day = moment(`${this.year}-${this.month}-${(index / 2) + 1}`).format("ddd")
      let date = (index / 2) + 1;
      worksheet.getColumn(6 + index).width = 10
      worksheet.getColumn(7 + index).width = 10

      worksheet.mergeCells(4, 6 + index, 4, index + 7)
      worksheet.getCell(4, 6 + index).value = day
      worksheet.getCell(4, 6 + index).font = {
        bold: true
      }
      worksheet.getCell(4, 6 + index).alignment = {
        vertical: "middle",
        horizontal: "center"
      }

      //for dates 
      worksheet.mergeCells(5, 6 + index, 5, index + 7)
      worksheet.getCell(5, 6 + index).value = date
      worksheet.getCell(5, 6 + index).font = {
        bold: true
      }
      worksheet.getCell(5, 6 + index).alignment = {
        vertical: "middle",
        horizontal: "center"
      }

      // for time in out
      worksheet.getCell(6, 6 + index).value = "Time In"
      worksheet.getCell(6, 6 + index).font = {
        bold: true
      }
      worksheet.getCell(6, 6 + index).alignment = {
        vertical: "middle",
        horizontal: "center"
      }

      worksheet.getCell(6, 7 + index).value = "Time Out"
      worksheet.getCell(6, 7 + index).font = {
        bold: true
      }
      worksheet.getCell(6, 7 + index).alignment = {
        vertical: "middle",
        horizontal: "center"
      }

      index += 2
    }

    worksheet.getColumn(totalDaysDouble + 6).width = 15
    worksheet.getColumn(totalDaysDouble + 7).width = 15
    worksheet.mergeCells(4, totalDaysDouble + 6, 5, totalDaysDouble + 7)
    worksheet.getCell(4, 6 + totalDaysDouble).value = "Total"
    worksheet.getCell(4, 6 + totalDaysDouble).font = {
      bold: true
    }
    worksheet.getCell(4, 6 + totalDaysDouble).alignment = {
      vertical: "middle",
      horizontal: "center"
    }

    worksheet.getCell(6, 6 + totalDaysDouble).value = "In Hours"
    worksheet.getCell(6, 6 + totalDaysDouble).font = {
      bold: true
    }
    worksheet.getCell(6, 6 + totalDaysDouble).alignment = {
      vertical: "middle",
      horizontal: "center"
    }

    worksheet.getCell(6, 7 + totalDaysDouble).value = "Over Time"
    worksheet.getCell(6, 7 + totalDaysDouble).font = {
      bold: true
    }
    worksheet.getCell(6, 7 + totalDaysDouble).alignment = {
      vertical: "middle",
      horizontal: "center"
    }
    /* Employee names and number - iteration through rows */

    this.attendanceArray.forEach((val, ind) => {

      worksheet.getCell(7 + ind, 1).value = `${val.first_name} ${val.last_name}`
      worksheet.getRow(7 + ind).alignment = {
        vertical: "middle",
        horizontal: "center"
      }

      let j = 0
      let totalTotalHours = 0
      let totalOverTime = 0
      while (totalDaysDouble > j) {

        let timeIn = val.attendance_array[j / 2]["time_in"]
        let timeOut = val.attendance_array[j / 2]["time_out"]
        let attendanceType = val.attendance_array[j / 2]["attedance_type"]
        let totalHours = val.attendance_array[j / 2]["service_hours"]
        let overTime = val.attendance_array[j / 2]["extra_hours"]

        if (attendanceType == 0) {
          worksheet.mergeCells(7 + ind, 6 + j, 7 + ind, 7 + j)
          worksheet.getCell(7 + ind, 6 + j).value = "Absent"
          worksheet.getCell(7 + ind, 6 + j).font = {
            bold: true,
            color: { argb: "FFFF0000" }
          }
        } else if (attendanceType == 1) {
          worksheet.getCell(7 + ind, 6 + j).value = timeIn
          worksheet.getCell(7 + ind, 7 + j).value = timeOut
          totalTotalHours += parseInt(totalHours)
          totalOverTime += parseInt(overTime)
        } else if (attendanceType == 3) {
          worksheet.mergeCells(7 + ind, 6 + j, 7 + ind, 7 + j)
          worksheet.getCell(7 + ind, 6 + j).value = "On Leave"
          worksheet.getCell(7 + ind, 6 + j).font = {
            bold: true,
            color: { argb: "FF6767EE" }
          }
        } else {
          worksheet.getCell(7 + ind, 6 + j).value = "-"
          worksheet.getCell(7 + ind, 7 + j).value = "-"

        }

        j += 2

      }
      worksheet.getCell(7 + ind, 6 + totalDaysDouble).value = totalTotalHours

      worksheet.getCell(7 + ind, 7 + totalDaysDouble).value = totalOverTime
    })


    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'CarData.xlsx');
    });
  }


  processAttendance(userAttendance) {
    if (userAttendance.attedance_type == 1) {
      return "Pr"
    } else if (userAttendance.attedance_type == 3) {
      return "Le"
    } else if (userAttendance.attedance_type == 0) {
      return "Ab"
    } else if (userAttendance.attedance_type == 2 && (moment(userAttendance.date).format('dddd') == "Saturday" || moment(userAttendance.date).format('dddd') == "Sunday")) {
      return "H"
    } else {
      return "-"
    }
  }

  open(content, attendanceDetail) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
    this.attendanceDetail = attendanceDetail
  }

  close() {
    this.modalService.dismissAll();
  }

  convertHours(hours) {
    return Math.floor(hours / 60) + 'h ' + hours % 60 + ' mins'
  }

}
