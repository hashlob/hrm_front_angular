import { Component, OnInit } from '@angular/core';
import { RoleService } from '../role.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss']
})
export class AddRoleComponent implements OnInit {
  constructor(
    private roleService: RoleService,
    private fb: FormBuilder,
    private router: Router,
    private toaster: ToastrService,
    private activeRoutes: ActivatedRoute
  ) { }

  formErrorStatus: boolean = false
  formError = ""
  id = null
  roleDetail
  roleForm = this.fb.group({
    name: ['', [Validators.required]],
    id: [this.id]
  })
  ngOnInit() {
    this.id = this.activeRoutes.snapshot.params.id
    if (this.id) {
      this.fetchRoleDetail()
    }
  }

  fetchRoleDetail() {
    this.roleService.getRoleById(this.id).
      then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.roleDetail = data
          this.setForm()
        }
      }).catch(err => {
        console.log(err)
      })
  }

  setForm() {
    this.roleForm.get('name').setValue(this.roleDetail.name)
  }

  addRole() {
    let formData = new FormData();
    if (this.id) {
      this.roleForm.get('id').setValue(this.id)
    }
    this.roleService.addRole(this.roleForm.value)
      .then((res: any) => {
        let { data, code, message } = res

        if (code == 200) {
          this.formErrorStatus = false
          this.toaster.success(message)
          this.router.navigate(['role/list'])
        }
      }).catch(err => {
        let { status, error } = err
        this.formErrorStatus = true
        if (status == 422) {
          this.formError = error.data.name[0]
          this.toaster.error(error.message)
        }
      })
  }

}
