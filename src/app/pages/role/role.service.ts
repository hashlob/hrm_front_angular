import { Injectable } from '@angular/core';
import AppConfig from "../../shared/app-config/appConfig"
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  baseUrl = AppConfig.API_ENDPOINT + "/role";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization",`Bearer ${this.token}`)
    this.headers = this.headers.append("Accept","application/json")
  }

  getAllRoles() {
   
    return this.http.get(this.baseUrl, {
      headers: this.headers
    }).toPromise()
  }

  addRole(payload) {
   
    return this.http.post(this.baseUrl, payload , {
      headers: this.headers
    }).toPromise()
  }

  deleteRole(payload) {
   
    return this.http.post(`${this.baseUrl}/delete`, payload , {
      headers: this.headers
    }).toPromise()
  }

  getRoleById(id) {
    return this.http.get(`${this.baseUrl}/getRoleById/${id}`, {
      headers: this.headers
    }).toPromise()
  }

}
