import { Component, OnInit } from '@angular/core';
import { RoleService } from '../role.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-role',
  templateUrl: './list-role.component.html',
  styleUrls: ['./list-role.component.scss']
})
export class ListRoleComponent implements OnInit {

  rows = [];
  columns = [
    { name: 'No' },
    { name: 'Title' },
    { name: 'Actions' }
  ];

  count = 0
  constructor(
    private roleService: RoleService,
    private toaster: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {

    this.roleService.getAllRoles()
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.rows = data.map((x, i) => {
            return {
              name: x.name,
              index: i,
              id: x.id,
              created_at: x.created_at,
              updated_at: x.updated_at
            }
          })
        }
      }).catch(err => {
        let { status, error = null } = err
        console.log(err)
      })
  }

  deleteRole(id) {
    let formData = new FormData;
    formData.append('id', id);
    this.roleService.deleteRole(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          let row = this.rows.filter(x => x.id != id)
          this.rows = row
          this.toaster.success(message)
        }
      }).catch(err => {
        console.log(err)
      })
  }

  editRole(id){
  this.router.navigate([`/role/add/${id}`])
  }

  getCounts(count) {
    return count + 1
  }
}
