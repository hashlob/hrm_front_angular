import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListRoleComponent } from './list-role/list-role.component';
import { AddRoleComponent } from './add-role/add-role.component';

const routes: Routes = [{
  path: "",
  children: [
    {
      path: "list",
      component: ListRoleComponent,
      data: {
        title: "Manage Roles"
      }
    },
    {
      path: "add",
      component: AddRoleComponent,
      data: {
        title: "Manage Roles"
      }
    },
    {
      path: "add/:id",
      component: AddRoleComponent,
      data: {
        title: "Manage Roles"
      }
    },

  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleRoutingModule { }
