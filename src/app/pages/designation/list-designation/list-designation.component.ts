import { Component, OnInit } from '@angular/core';
import { DesignationService } from '../designation.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-designation',
  templateUrl: './list-designation.component.html',
  styleUrls: ['./list-designation.component.scss']
})
export class ListDesignationComponent implements OnInit {
  rows = [];
  columns = [
    { name: 'No' },
    { name: 'Title' },
    { name: 'Actions' }
  ];
  constructor(
    private desiganationService: DesignationService,
    private toaster: ToastrService,
    private router: Router
  ) { }

  loadingState = true
  ngOnInit() {
    this.desiganationService.getAllDesignations()
      .then((res: any) => {
        this.loadingState = false
        let { data, code, message } = res
        if (code == 200) {
          this.rows = res.data
        }
      }).catch(err => {
        let { status, error = null } = err
        this.loadingState = false
        console.log(err)
      })
  }

  deleteDesignation(id) {
    let formData = new FormData;
    formData.append('id', id);
    this.desiganationService.deleteDesignation(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          let row = this.rows.filter(x => x.id != id)
          this.rows = row
          this.toaster.success(message)
        }
      }).catch(err => {
        console.log(err)
      })
  }

}
