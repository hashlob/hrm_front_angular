import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DesignationRoutingModule } from './designation-routing.module';
import { AddDesignationComponent } from './add-designation/add-designation.component';
import { ListDesignationComponent } from './list-designation/list-designation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [AddDesignationComponent, ListDesignationComponent],
  imports: [
    CommonModule,
    DesignationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ]
})
export class DesignationModule { }
