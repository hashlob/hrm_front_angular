
import { Component, OnInit } from '@angular/core';
import { DesignationService } from '../designation.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-designation',
  templateUrl: './add-designation.component.html',
  styleUrls: ['./add-designation.component.scss']
})
export class AddDesignationComponent implements OnInit {

  constructor(
    private designationService : DesignationService,
    private fb: FormBuilder,
    private router: Router,
    private toaster: ToastrService) { }
  
  dessignationForm = this.fb.group({
    title: ['', [Validators.required]]
  })
  formErrorStatus: boolean = false
  formError = ""
  ngOnInit() {
  }
  addDesignation() {
    let formData = new FormData();
    formData.append("title", this.dessignationForm.value.title)
    this.designationService.addDesognation(formData)
      .then((res: any) => {
        let { data, code, message } = res

        if (code == 200) {
          this.formErrorStatus = false
          this.toaster.success(message)
          this.router.navigate(['designation/list'])
        }
      }).catch(err => {
        let { status, error } = err
        this.formErrorStatus = true
        if (status == 422) {
          this.formError = error.data.name[0]
          this.toaster.error(error.message)
        }
      })
  }
}
