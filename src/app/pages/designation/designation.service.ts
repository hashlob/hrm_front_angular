import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DesignationService {

  baseUrl = AppConfig.API_ENDPOINT + "/designation";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization",`Bearer ${this.token}`)
    this.headers = this.headers.append("Accept","application/json")
  }
  getAllDesignations() {
   
    return this.http.get(this.baseUrl, {
      headers: this.headers
    }).toPromise()
  }

  addDesognation(payload) {
   
    return this.http.post(this.baseUrl, payload , {
      headers: this.headers
    }).toPromise()
  }

  deleteDesignation(payload) {
   
    return this.http.post(`${this.baseUrl}/delete`, payload , {
      headers: this.headers
    }).toPromise()
  }

  getDesignationById(id) {
    return this.http.get(`${this.baseUrl}/getDesignationById/${id}`, {
      headers: this.headers
    }).toPromise()
  }

}
