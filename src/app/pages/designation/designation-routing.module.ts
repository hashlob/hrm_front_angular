import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListDesignationComponent } from './list-designation/list-designation.component';
import { AddDesignationComponent } from './add-designation/add-designation.component';

const routes: Routes = [
  {
    path : "",
    children : [
      {
        path : "list",
        component : ListDesignationComponent
      },
      {
        path : "add",
        component : AddDesignationComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesignationRoutingModule { }
