import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemVariableRoutingModule } from './system-variable-routing.module';

import { ListSystemVariableComponent } from './list-system-variable/list-system-variable.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [ ListSystemVariableComponent],
  imports: [
    CommonModule,
    SystemVariableRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgbDropdownModule
  ]
})
export class SystemVariableModule { }
