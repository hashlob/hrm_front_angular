import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListSystemVariableComponent } from './list-system-variable/list-system-variable.component';

const routes: Routes = [
{
  path : '',
  children : [{
    path : 'list',
    component : ListSystemVariableComponent
  }]
}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemVariableRoutingModule { }
