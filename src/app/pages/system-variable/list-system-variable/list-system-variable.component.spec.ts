import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSystemVariableComponent } from './list-system-variable.component';

describe('ListSystemVariableComponent', () => {
  let component: ListSystemVariableComponent;
  let fixture: ComponentFixture<ListSystemVariableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSystemVariableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSystemVariableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
