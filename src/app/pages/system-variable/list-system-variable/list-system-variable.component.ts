import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-system-variable',
  templateUrl: './list-system-variable.component.html',
  styleUrls: ['./list-system-variable.component.scss']
})
export class ListSystemVariableComponent implements OnInit {
  rows = [];
  columns = [
    { name: 'No' },
    { name: 'Option' },
    { name: 'Actions' }

  ];

  optionDetails = []
  DBVariables = []
  optionDetail
  id = null

  constructor(
    private sharedService: SharedService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toaster: ToastrService
  ) { }

  optionForm = this.fb.group({
    type_id: ['', [Validators.required]],
    options: ['', [Validators.required]],
  })

  ngOnInit() {
    this.fetchDBVaiables()
  }

  open(content, id = null) {
    this.optionForm.reset()
    if (id) {
      this.id = id
      this.fetchOptionById(id)
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  fetchOptionById(id) {
    this.sharedService.fetchOptionById({ id })
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.optionDetail = data

        }
      }).catch(err => {
        console.log(err)
      })
  }

  setForm() {
    this.optionForm.get("type_id").setValue(this.optionDetail.type_id)
    this.optionForm.get("options").setValue(this.optionDetail.options)
  }

  fetchDBVaiables() {
    this.sharedService.getSystemVariable()
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.DBVariables = data
        }
      }).catch(err => {
        console.log(err)
      })
  }

  onChangeSelect(event) {
    this.rows = []
    this.fetchDBVaiablesDetails(event.target.value)
  }

  fetchDBVaiablesDetails(type_id) {
    this.sharedService.getDBVariable({ type_id })
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.optionDetails = data[0].details
        }
      }).catch(err => {
        console.log(err)
      })
  }

  addOptionDetails() {
    let formData = new FormData;

    formData.append('options', this.optionForm.value.options)
    formData.append('type_id', this.optionForm.value.type_id)
    if (this.id != null) {
      formData.append('id', this.id)
    }
    this.sharedService.createSytemVarable(formData)
      .then((res: any) => {
        let { code, message, data } = res
        this.close()
        this.fetchDBVaiablesDetails(this.optionForm.value.type_id)
        this.toaster.success(message)
        this.id = null
        this.optionForm.reset()

      }).catch(err => {
        console.log(err)
      })
  }

}
