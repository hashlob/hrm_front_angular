import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRequestLineComponent } from './list-request-line.component';

describe('ListRequestLineComponent', () => {
  let component: ListRequestLineComponent;
  let fixture: ComponentFixture<ListRequestLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRequestLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRequestLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
