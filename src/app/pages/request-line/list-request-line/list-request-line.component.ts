import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { SharedService } from 'app/shared/shared.service';
import AppConfig from 'app/shared/app-config/appConfig';
import { RequestLineService } from '../request-line.service';

@Component({
  selector: 'app-list-request-line',
  templateUrl: './list-request-line.component.html',
  styleUrls: ['./list-request-line.component.scss']
})
export class ListRequestLineComponent implements OnInit {

  constructor(
    private toaster: ToastrService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private sharedService: SharedService,
    private requestLineService: RequestLineService
  ) { }
  columns = [
    { name: 'No' },
    { name: 'Line Name' },
    { name: 'Manager' },
    { name: 'Department' },
    { name: 'Order' },
    { name: 'Actions' },
  ];

  requestLineForm = this.fb.group({
    line_name: ['', [Validators.required]],
    manager_id: ['', [Validators.required]],
    department_id: ['', [Validators.required]],
    order: ['', [Validators.required]],
    request_line_type: ['', [Validators.required]],
  })

  DBVariables = []
  managers = []
  departments = []
  requestLineType = null
  departmentId = null
  rows = []

  ngOnInit() {
    this.fetchRequestLineTypes()
    this.getDepartmentForSelect()
    this.getUsersForSelect()

  }

  getUsersForSelect() {
    this.sharedService.getUsersForSelect({ requestLine: true })
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.managers = data
        }
      }).catch(err => {
        console.log(err)
      })
  }

  open(content, id = null) {
    this.requestLineForm.reset()
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }


  fetchRequestLineTypes() {
    this.sharedService.getDBVariable({ type_id: AppConfig.DBVariablesId.RequestLineType })
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.DBVariables = data[0].details
        }
      }).catch(err => {
        console.log(err)
      })
  }

  getDepartmentForSelect() {
    this.sharedService.getDepartmentForSelect({})
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.departments = data
        }
      }).catch(err => {
        console.log(err)
      })
  }

  onChangeSelect(event) {
    this.requestLineType = event.target.value
  }

  onChangeSelectDepartment(event) {
    this.departmentId = event.target.value
  }

  fetchRequestLines() {
    let params = {
      departId: this.departmentId,
      requestLineType: this.requestLineType
    }
    this.requestLineService.fetchRequestLines(params)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200 && data.length > 0) {
          this.rows = data
        } else {
          this.toaster.info("No request line found.")
        }
      }).catch(err => {
        console.log(err)
      })
  }

  addRequestLine() {
    this.requestLineService.addRequestLine(this.requestLineForm.value)
      .then((res: any) => {
        let { code, data, message } = res
        debugger
        if (code == 200) {

          this.close()
          this.requestLineForm.reset();
          this.toaster.success(message)
          this.fetchRequestLines()
        }
      }).catch(err => {
        console.log(err)
      })
  }

  delete(id) {
    this.requestLineService.delete(id)
      .then((res: any) => {
        let { datat, code, message } = res
        if (code == 200) {
          let filtered = this.rows.filter(x => x.id != id)
          this.toaster.success(message)
          this.rows = filtered
        }
      }).catch(err => {
        console.log(err)
      })
  }

}
