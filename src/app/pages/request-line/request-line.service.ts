import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestLineService {

  baseUrl = AppConfig.API_ENDPOINT + "/department";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  fetchRequestLines(params) {
    return this.http.get(`${this.baseUrl}/requestLine/getForDepartment`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  addRequestLine(payload) {
    return this.http.post(`${this.baseUrl}/requestLine`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  delete(id) {
    return this.http.get(`${this.baseUrl}/requestLine/delete/${id}`, {
      headers: this.headers,
    }).toPromise()
  }
}
