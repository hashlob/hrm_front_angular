import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListRequestLineComponent } from './list-request-line/list-request-line.component';

const routes: Routes = [
  {
    path: '',
    children: [{
      path: 'list',
      data: {
        title: "Request Line",
      },
      component: ListRequestLineComponent
    }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestLineRoutingModule { }
