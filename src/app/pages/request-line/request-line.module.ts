import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestLineRoutingModule } from './request-line-routing.module';
import { ListRequestLineComponent } from './list-request-line/list-request-line.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [ListRequestLineComponent],
  imports: [
    CommonModule,
    RequestLineRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbDropdownModule,
    NgxDatatableModule
  ]
})
export class RequestLineModule { }
