import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HiringManagementService } from '../hiring-management.service';
import AppConfig from 'app/shared/app-config/appConfig';

@Component({
  selector: 'app-job-posts',
  templateUrl: './job-posts.component.html',
  styleUrls: ['./job-posts.component.scss']
})
export class JobPostsComponent implements OnInit {

  columns = [
    { name: 'No' },
    { name: 'Job Title' },
    // { name: 'Job Description' },
    { name: 'Added on' },
    { name: 'Added Uptil' },
    { name: 'Department' },
    { name: 'Status' },
    { name: 'Actions' }

  ];

  jobPosts = []
  departmemts = []
  jobStatus = [
    {
      name: "Active",
      id: '1'
    },
    {
      name: "Inactive",
      id: 0
    }
  ]
  manager_id = null
  id = null
  jobPost = null

  constructor(
    private sharedService: SharedService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toaster: ToastrService,
    private hiringService: HiringManagementService
  ) { }

  ngOnInit() {
    this.fetchDepartments()
    this.getAllJobPosts()
    this.manager_id = AppConfig.getUserId()

  }

  jobPostForm = this.fb.group({
    job_title: ['', [Validators.required]],
    job_description: ['', [Validators.required]],
    from: ['', [Validators.required]],
    to: ['', [Validators.required]],
    department_id: ['', [Validators.required]],
    status: ['', [Validators.required]],
    manager_id: [''],
    id: ['']
  })

  open(content, id = null) {
    this.jobPostForm.reset()
    if (id) {
      this.id = id
      this.fetchJobPostById(id)
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  fetchJobPostById(id) {
    this.hiringService.getJobPostById(id)
      .then((res: any) => {
        let { message, data, code } = res
        if (code == 200) {
          this.jobPost = data
          this.setForm()
        }
      }).catch(err => console.log(err))
  }

  setForm() {
    this.jobPostForm.get("job_title").setValue(this.jobPost.job_title)
    this.jobPostForm.get("job_description").setValue(this.jobPost.job_description)
    this.jobPostForm.get("from").setValue(this.jobPost.from)
    this.jobPostForm.get("to").setValue(this.jobPost.to)
    this.jobPostForm.get("manager_id").setValue(this.jobPost.manager_id)
    this.jobPostForm.get("department_id").setValue(this.jobPost.department_id)
    this.jobPostForm.get("status").setValue(this.jobPost.status)
    this.jobPostForm.get("id").setValue(this.id)
  }

  getAllJobPosts() {
    this.hiringService.getAllJobPosts({})
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.jobPosts = data
        }
      })
  }

  fetchDepartments() {

    this.sharedService.getDepartmentForSelect({})
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.departmemts = data
        }
      }).catch(err => console.log(err))
  }

  changeStatus(id, status) {
    let payload = {
      id,
      status
    }
    this.hiringService.changeStatus(payload)
      .then((res: any) => {
        let { data, code, message } = res

        if (code == 200) {
          this.toaster.success(message)

          let index = this.jobPosts.findIndex(x => x.id == id)
          let filtered = this.jobPosts[index]
          filtered.status = status
          this.jobPosts[index] = filtered

        }
      })
  }

  addjobPosts() {

    this.hiringService.addJobPost(this.jobPostForm.value)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.toaster.success(message)
          this.jobPostForm.reset()
          this.getAllJobPosts()
          this.id = null
          this.close()
        }
      }).catch(err => console.log(err))
  }

}
