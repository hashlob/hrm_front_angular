import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobPostsComponent } from './job-posts/job-posts.component';
import { CvBankComponent } from './cv-bank/cv-bank.component';
import { InterviewComponent } from './interview/interview.component';
import { HiringRequestsComponent } from './hiring-requests/hiring-requests.component';

const routes: Routes = [{
  path: "",
  children: [
    {
      path: "jobposts",
      component: JobPostsComponent
    },
    {
      path: "cvbank",
      component: CvBankComponent
    },
    {
      path: 'interviews',
      component: InterviewComponent
    },
    {
      path: 'hiringrequests',
      component: HiringRequestsComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HiringManagementRoutingModule { }
