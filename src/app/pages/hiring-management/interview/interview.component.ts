import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HiringManagementService } from '../hiring-management.service';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.scss']
})
export class InterviewComponent implements OnInit {

  constructor(
    private sharedService: SharedService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toaster: ToastrService,
    private hiringService: HiringManagementService
  ) { }

  columns = [
    { name: 'No' },
    { name: 'Interview date' },
    { name: 'Interview time' },
    { name: 'Test details' },
    { name: 'Interview details' },
    { name: 'CV' },
    { name: 'Panel' },
    { name: 'Status' },
    { name: 'Actions' }
  ]
  interviewForm = this.fb.group({
    interview_date: ['', [Validators.required]],
    interview_time: ['', [Validators.required]],
    test_details: ['', [Validators.required]],
    interview_details: ['', [Validators.required]],
    id: [''],
    interview_panel: ['', [Validators.required]]
  })
  users = []
  inteviews = []
  inteview = null
  id = null

  ngOnInit() {
    this.fetchUsers()
    this.fetchInterviews()

  }

  fetchUsers() {
    this.sharedService.getUsersForSelect({ all: true })
      .then((res: any) => {
        let { data, message, code } = res

        if (code == 200) {
          this.users = data.map(x => {
            return {
              name: x.first_name + " " + x.last_name,
              id: x.id
            }
          })
        }
      }).catch(err => console.log(err))
  }

  open(content, id = null) {
    this.interviewForm.reset()
    this.id = id
    this.fetchInterviewById(id)
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  fetchInterviews() {
    this.hiringService.fetchInterviews({})
      .then((res: any) => {
        let { data, message, code } = res
        if (code == 200) {
          this.inteviews = data
        }
      }).catch(err => console.log(err))
  }

  fetchInterviewById(id) {
    this.hiringService.fetchInterviewById(id)
      .then((res: any) => {
        let { data, message, code } = res
        if (code == 200) {
          this.inteview = data
          this.setForm()

        }
      }).catch(err => console.log(err))
  }
  setForm() {
    let panel = this.inteview.panel.map(x => x.interviewer_id)
    this.interviewForm.get("interview_date").setValue(this.inteview.interview_date)
    this.interviewForm.get("interview_time").setValue(this.inteview.interview_time)
    this.interviewForm.get("test_details").setValue(this.inteview.test_details)
    this.interviewForm.get("interview_details").setValue(this.inteview.interview_details)
    this.interviewForm.get("interview_panel").setValue(panel)
    this.interviewForm.get("id").setValue(this.id)
  }

  addinterviews() {
    this.hiringService.addInterview(this.interviewForm.value)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.toaster.success(message)
          this.close()
          this.id = null
          this.interviewForm.reset()
          this.fetchInterviews()
        }
      }).catch(err => {
        let { status, error } = err
        this.toaster.error(error.message)
      })
  }

  changeStatus(id, status) {
    let obj = {
      id,
      status,
    }
    this.hiringService.changeInterviewStatus(obj)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {
          let index = this.inteviews.findIndex(x => x.id == id)
          let filtered = this.inteviews[index]
          filtered.status = status
          this.toaster.success(message)
          this.inteviews[index] = filtered
        }
      }).catch(err => console.log(err))
  }

}
