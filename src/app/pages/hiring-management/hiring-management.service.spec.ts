import { TestBed } from '@angular/core/testing';

import { HiringManagementService } from './hiring-management.service';

describe('HiringManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HiringManagementService = TestBed.get(HiringManagementService);
    expect(service).toBeTruthy();
  });
});
