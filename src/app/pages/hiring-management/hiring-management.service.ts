import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HiringManagementService {

  baseUrl = AppConfig.API_ENDPOINT + "/jobPost";
  hiringBaseUrl = AppConfig.API_ENDPOINT + "/hiringRequests";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  getAllJobPosts(params) {
    return this.http.get(`${this.baseUrl}`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  getJobPostById(id) {
    return this.http.get(`${this.baseUrl}/getJobPostById/${id}`, {
      headers: this.headers,
    }).toPromise()
  }

  changeStatus(payload) {
    return this.http.post(`${this.baseUrl}/changeStatus`, payload, {
      headers: this.headers
    }).toPromise()

  }

  addJobPost(payload) {
    return this.http.post(`${this.baseUrl}`, payload, {
      headers: this.headers
    }).toPromise()
  }

  addCv(payload) {
    return this.http.post(`${this.baseUrl}/uploadCv`, payload, {
      headers: this.headers
    }).toPromise()
  }

  getAllCvs(params) {
    return this.http.get(`${this.baseUrl}/getAllCvs`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  getCv(params) {
    return this.http.get(`${this.baseUrl}/downloadCv`, {
      headers: this.headers,
      responseType: 'blob',
      params
    }).toPromise()
  }

  changeCvStatus(payload) {
    return this.http.post(`${this.baseUrl}/cv/changeStatus`, payload, {
      headers: this.headers
    }).toPromise()

  }

  fetchInterviews(params) {
    return this.http.get(`${this.baseUrl}/getAllInterviews`, {
      headers: this.headers,
      params
    }).toPromise()
  }


  fetchInterviewById(id) {
    return this.http.get(`${this.baseUrl}/fetchInterviewById/${id}`, {
      headers: this.headers,
    }).toPromise()
  }

  addInterview(payload) {
    return this.http.post(`${this.baseUrl}/scheduleInterview`, payload, {
      headers: this.headers
    }).toPromise()
  }

  changeInterviewStatus(payload) {
    return this.http.post(`${this.baseUrl}/interview/changeStatus`, payload, {
      headers: this.headers
    }).toPromise()

  }

  getAllHiringRequests(params) {
    return this.http.get(`${this.hiringBaseUrl}`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  addHiringRequests(payload) {
    return this.http.post(`${this.hiringBaseUrl}/`, payload, {
      headers: this.headers
    }).toPromise()
  }

  fetchHiringById(id) {
    return this.http.get(`${this.hiringBaseUrl}/fetchHiringById/${id}`, {
      headers: this.headers,
    }).toPromise()
  }

  changeHiringStatus(payload) {
    return this.http.post(`${this.hiringBaseUrl}/processHiringRequest`, payload, {
      headers: this.headers
    }).toPromise()

  }
}
