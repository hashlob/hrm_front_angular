import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HiringManagementRoutingModule } from './hiring-management-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModalModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { JobPostsComponent } from './job-posts/job-posts.component';
import { CvBankComponent } from './cv-bank/cv-bank.component';
import { InterviewComponent } from './interview/interview.component';
import { HiringRequestsComponent } from './hiring-requests/hiring-requests.component';

@NgModule({
  declarations: [JobPostsComponent, CvBankComponent, InterviewComponent, HiringRequestsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgSelectModule,
    NgbModalModule,
    NgbDropdownModule,
    HiringManagementRoutingModule
  ]
})
export class HiringManagementModule { }
