import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HiringManagementService } from '../hiring-management.service';
import AppConfig from 'app/shared/app-config/appConfig';

@Component({
  selector: 'app-cv-bank',
  templateUrl: './cv-bank.component.html',
  styleUrls: ['./cv-bank.component.scss']
})
export class CvBankComponent implements OnInit {

  columns = [
    { name: 'No' },
    { name: 'Name' },
    { name: 'Added on' },
    { name: 'Department' },
    { name: 'Status' },
    { name: 'Actions' }

  ];
  departmemts = []
  cvs = []
  cv = null
  user_id = null
  users = []
  cv_id = null

  constructor(
    private sharedService: SharedService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toaster: ToastrService,
    private hiringService: HiringManagementService
  ) { }

  cvForm = this.fb.group({
    cv: [''],
    name: ['', [Validators.required]],
    date: ['', [Validators.required]],
    department_id: ['', [Validators.required]],
  })

  interviewForm = this.fb.group({
    interview_date: ['', [Validators.required]],
    interview_time: ['', [Validators.required]],
    test_details: ['', [Validators.required]],
    interview_details: ['', [Validators.required]],
    cv_id: [''],
    interview_panel: ['', [Validators.required]]
  })

  ngOnInit() {
    this.fetchDepartments()
    this.fetchUsers()
    this.getAllCvs()
    // this.user_id = AppConfig.getUserId()
    this.user_id = 2
  }

  open(content, id = null) {
    this.cvForm.reset()
    this.interviewForm.reset()
    this.cv = null
    if (id) {
      this.cv_id = id
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  fetchUsers() {
    this.sharedService.getUsersForSelect({ all: true })
      .then((res: any) => {
        let { data, message, code } = res

        if (code == 200) {
          this.users = data.map(x => {
            return {
              name: x.first_name + " " + x.last_name,
              id: x.id
            }
          })
        }
      }).catch(err => console.log(err))
  }

  getAllCvs() {
    this.hiringService.getAllCvs({})
      .then((res: any) => {
        let { data, message, code } = res

        if (code == 200) {
          this.cvs = data
        }
      }).catch(err => console.log(err))
  }

  fetchDepartments() {

    this.sharedService.getDepartmentForSelect({})
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.departmemts = data
        }
      }).catch(err => console.log(err))
  }

  downloadCv(path) {
    this.hiringService.getCv({ path })
      .then((res: any) => {
        const url = window.URL.createObjectURL(res);
        window.open(url);
      })
  }

  processCv(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if (file) {
        if ((/\.(jpg|jpeg|png|docx|pdf)$/i).test(file.name) && file.size <= 2000000) {
          this.cvForm.controls['cv'].setErrors(null);
          this.cv = file;
          console.log(file)
        } else {
          this.cvForm.controls['cv'].setErrors({ invalid: true });
        }
      }
    }
  }

  addCvPosts() {
    let formData = new FormData;
    formData.append("name", this.cvForm.value.name)
    formData.append("date", this.cvForm.value.date)
    formData.append("department_id", this.cvForm.value.department_id)
    formData.append("cv", this.cv)

    this.hiringService.addCv(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.close()
          this.cvForm.reset()
        }
      }).catch(err => console.log(err))
  }

  changeStatus(id, status, department_id) {
    let obj = {
      id,
      status,
      user_id: this.user_id,
      department_id
    }
    this.hiringService.changeCvStatus(obj)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {
          let index = this.cvs.find(x => x.id == id)
          let filtered = this.cvs[index]
          if (this.user_id == 2) {
            filtered.status = "2"
          } else {
            filtered.status = status
          }
          this.toaster.success(message)
          this.cvs[index] = filtered
        }
      }).catch(err => console.log(err))
  }

  addinterviews() {
    this.interviewForm.get('cv_id').setValue(this.cv_id)
    this.hiringService.addInterview(this.interviewForm.value)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.toaster.success(message)
          this.close()
          this.cv_id = null
          this.interviewForm.reset()
        }
      }).catch(err => {
        let { status, error } = err
        this.toaster.error(error.message)
      })
  }

}
