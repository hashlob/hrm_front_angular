import { Component, OnInit } from '@angular/core';
import { HiringManagementService } from '../hiring-management.service';
import { SharedService } from 'app/shared/shared.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import AppConfig from 'app/shared/app-config/appConfig';

@Component({
  selector: 'app-hiring-requests',
  templateUrl: './hiring-requests.component.html',
  styleUrls: ['./hiring-requests.component.scss']
})
export class HiringRequestsComponent implements OnInit {

  hiringDetails = []
  columns = [
    { name: 'No' },
    { name: 'Job title' },
    { name: 'Job description' },
    { name: 'Reason' },
    { name: 'Date' },
    { name: 'Department' },
    { name: 'Status' },
    { name: 'Actions' }

  ];
  role_id = null
  department_id = null
  manager_id = null
  id
  hiringDetail = null
  constructor(
    private sharedService: SharedService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toaster: ToastrService,
    private hiringService: HiringManagementService
  ) { }

  hiringForm = this.fb.group({
    job_title: ['', [Validators.required]],
    job_description: ['', [Validators.required]],
    reason: ['', [Validators.required]],
    date: ['', [Validators.required]],
    department_id: [''],
    manager_id: [''],
    id: ['']
  })

  ngOnInit() {
    // this.role_id = AppConfig.getRoleId()
    this.role_id = 2
    // this.department_id = AppConfig.getDepartmentId()
    this.department_id = 1
    this.manager_id = AppConfig.getUserId()
    this.getAllHiringRequests()

  }

  getAllHiringRequests() {
    let params = {
      role_id: this.role_id,
      department_id: this.department_id,
      manager_id: this.manager_id
    }
    this.hiringService.getAllHiringRequests(params)
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.hiringDetails = data
          debugger
        }
      }).catch(err => console.log(err))
  }

  open(content, id = null) {
    this.hiringForm.reset()
    if (id) {
      this.id = id
      this.fetchHiringById(id)
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  fetchHiringById(id) {
    this.hiringService.fetchHiringById(id)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.hiringDetail = data
          debugger
          this.setForm()
        }
      }).catch(err => console.log(err))
  }
  setForm() {
    this.hiringForm.get('job_title').setValue(this.hiringDetail.job_title)
    this.hiringForm.get('job_description').setValue(this.hiringDetail.job_description)
    this.hiringForm.get('reason').setValue(this.hiringDetail.reason)
    this.hiringForm.get('date').setValue(this.hiringDetail.date)
    this.hiringForm.get('id').setValue(this.hiringDetail.id)
  }

  addhiringDetails() {
    this.hiringForm.get("department_id").setValue(this.department_id);
    this.hiringForm.get("manager_id").setValue(this.manager_id)

    this.hiringService.addHiringRequests(this.hiringForm.value)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.close()
          this.id
          this.toaster.success(message)
          this.hiringForm.reset()
          this.getAllHiringRequests()
        }
      }).catch(err => {
        console.log(err)
      })
  }

  changeStatus(id, status) {
    let payload = {
      id,
      status
    }
    this.hiringService.changeHiringStatus(payload)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          let index = this.hiringDetails.findIndex(x => x.id == id)
          let filtered = this.hiringDetails[index]
          filtered.status = status
          this.hiringDetails[index] = filtered
          this.toaster.success(message)
          this.id = null
        }
      }).catch(err => console.log(err))
  }

}
