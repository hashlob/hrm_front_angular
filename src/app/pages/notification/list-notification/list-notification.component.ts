import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import AppConfig from 'app/shared/app-config/appConfig';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-notification',
  templateUrl: './list-notification.component.html',
  styleUrls: ['./list-notification.component.scss']
})
export class ListNotificationComponent implements OnInit {

  notifications = []
  user_id = null
  constructor(
    private sharedService: SharedService,
    private router: Router
  ) { }

  ngOnInit() {
    this.user_id = AppConfig.getUserId()
    this.getAllNotificatons(this.user_id)
  }

  getAllNotificatons(user_id) {
    this.sharedService.getNotifications({ user_id, all: true })
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.notifications = data
        }
      }).catch(err => console.log(err))
  }

  readNotification(id, path) {
    this.sharedService.readNotifications(id)
      .then((res: any) => {
        this.router.navigate([path])
      }).catch(err => console.log(err))
  }

}
