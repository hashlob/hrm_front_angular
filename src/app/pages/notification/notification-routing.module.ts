import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListNotificationComponent } from './list-notification/list-notification.component';

const routes: Routes = [{
  path: "",
  data: {
    title: "List Notification"
  },
  children: [
    {
      path: 'list',
      component: ListNotificationComponent,
      data: {
        title: "List Notification"
      }
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationRoutingModule { }
