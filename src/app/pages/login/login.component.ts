import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [ToastrService]
})
export class LoginComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private toaster: ToastrService,
    private router: Router
  ) { }
  loginForm = this.fb.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]]
  })
  ngOnInit() {

  }

  onLogin() {
    let formData = new FormData();
    formData.append("username", this.loginForm.value.username)
    formData.append("password", this.loginForm.value.password)
    this.loginService.login(formData)
      .then((res: any) => {
        let { token, user, userSystemDetails, code, message = null } = res
        console.log(res)
        if (code == 200) {
          debugger
          this.toaster.success("Login Successful.")
          localStorage.setItem('token', token)
          localStorage.setItem('user', JSON.stringify(user))
          localStorage.setItem('permissions', JSON.stringify(user.system_detail.role.permissions))
          localStorage.setItem('userSystemDetail', JSON.stringify(userSystemDetails))

          this.router.navigate(['dashboard'])

        } else if (code == 422) {
          this.toaster.error(message)
        } else {
          this.toaster.info(message)
        }
      }).catch(err => console.log(err))
  }

}
