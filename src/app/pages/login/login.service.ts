import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import AppConfig from "../../shared/app-config/appConfig"

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseUrl = AppConfig.API_ENDPOINT + "/auth";
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {

    this.headers = this.headers.append("Accept","application/json")
  }

  login(payload) {

    return this.http.post(`${this.baseUrl}/login`, payload, {
      headers: this.headers
    }).toPromise()
  }
}
