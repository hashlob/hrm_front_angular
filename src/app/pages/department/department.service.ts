import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import AppConfig from 'app/shared/app-config/appConfig';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  baseUrl = AppConfig.API_ENDPOINT + "/department";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  getAllDepartments() {
    return this.http.get(this.baseUrl, {
      headers: this.headers
    }).toPromise()
  }

  addDepartment(payload) {
    return this.http.post(this.baseUrl, payload, {
      headers: this.headers
    }).toPromise()
  }

  deleteDepartment(payload) {
    return this.http.post(`${this.baseUrl}/delete`, payload, {
      headers: this.headers
    }).toPromise()
  }

  getDepartmentById(id) {
    return this.http.get(`${this.baseUrl}/getDepartmentById/${id}`, {
      headers: this.headers
    }).toPromise()
  }
}
