import { Component, OnInit } from '@angular/core';
import { DepartmentService } from '../department.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-departments',
  templateUrl: './list-departments.component.html',
  styleUrls: ['./list-departments.component.scss']
})
export class ListDepartmentsComponent implements OnInit {

  rows = [];
  loadingState = true
  columns = [
    { name: 'No' },
    { name: 'Name' },
    { name: 'Code' },
    { name: 'Prefix' },
    { name: 'Manager' },
    { name: 'Actions' }
  ];

  count = 0
  constructor(
    private departmentService: DepartmentService,
    private toaster: ToastrService,
    private router: Router

  ) { }

  ngOnInit() {

    this.departmentService.getAllDepartments()
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.loadingState = false
          this.rows = data
        }
      }).catch(err => {
        let { status, error = null } = err
        console.log(err)
      })
  }

  editDepartment(id) {
    this.router.navigate([`/department/add/${id}`])

  }

  deleteDepartment(id) {
    let formData = new FormData;
    formData.append('id', id);
    this.departmentService.deleteDepartment(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          let row = this.rows.filter(x => x.id != id)
          this.rows = row
          this.toaster.success(message)
        }
      }).catch(err => {
        console.log(err)
      })
  }

  getCounts(index) {
    return index + 1
  }
}
