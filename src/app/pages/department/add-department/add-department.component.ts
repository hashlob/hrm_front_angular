import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DepartmentService } from '../department.service';
import { SharedService } from 'app/shared/shared.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.scss']
})
export class AddDepartmentComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private departmentService: DepartmentService,
    private sharedService: SharedService,
    private router: Router,
    private toaster: ToastrService,
    private activeRoutes: ActivatedRoute

  ) { }

  departments = []
  managers = []

  departmentForm = this.fb.group({
    name: ['', [Validators.required]],
    code: [''],
    prefix: [''],
    manager_id: [''],
    parent_id: [''],
  })

  formErrorStatus: boolean = false
  formError = ""
  departmentDetail
  id

  ngOnInit() {
    this.id = this.activeRoutes.snapshot.params.id
    if (this.id) {
      this.fetchDepartmentDetail()
    }
    this.fetchDepartments();
    this.fetchManagers();

  }

  fetchDepartmentDetail() {
    this.departmentService.getDepartmentById(this.id).
      then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.departmentDetail = data
          this.setForm()
        }
      }).catch(err => {
        console.log(err)
      })
  }

  setForm() {
    this.departmentForm.get('name').setValue(this.departmentDetail.name)
    this.departmentForm.get('code').setValue(this.departmentDetail.code)
    this.departmentForm.get('prefix').setValue(this.departmentDetail.prefix)
    this.departmentForm.get('manager_id').setValue(this.departmentDetail.manager_id)
    this.departmentForm.get('parent_id').setValue(this.departmentDetail.parent_id)
  }

  fetchDepartments() {
    this.departmentService.getAllDepartments()
      .then((res: any) => {
        let { data, code, message } = res

        if (code == 200) {
          this.departments = data
        }
      }).catch(err => {
        let { status, error = null } = err
        console.log(err)
      })
  }

  fetchManagers() {
    this.sharedService.getUsersForSelect({ userType: 3 })
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.managers = data
        }
      }).catch(err => {
        let { status, error = null } = err
        console.log(err)
      })
  }

  addDepartment() {
    let formData = new FormData();
    formData.append("name", this.departmentForm.value.name)
    formData.append("code", this.departmentForm.value.code)
    formData.append("prefix", this.departmentForm.value.prefix)
    formData.append("manager_id", this.departmentForm.value.manager_id)
    formData.append("parent_id", this.departmentForm.value.parent_id)

    if (this.id) {
      formData.append('id', this.id)
    }

    this.departmentService.addDepartment(formData)
      .then((res: any) => {
        let { data, code, message } = res

        if (code == 200) {
          this.formErrorStatus = false
          this.toaster.success(message)
          this.router.navigate(['department/list'])
        }
      }).catch(err => {
        let { status, error } = err
        this.formErrorStatus = true
        if (status == 422) {
          this.formError = error.data.name[0]
          this.toaster.error(error.message)
        }
      })
  }

}
