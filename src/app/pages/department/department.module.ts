import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartmentRoutingModule } from './department-routing.module';
import { ListDepartmentsComponent } from './list-departments/list-departments.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AddDepartmentComponent } from './add-department/add-department.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [ListDepartmentsComponent, AddDepartmentComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgxDatatableModule,
    DepartmentRoutingModule,
    NgbDropdownModule
  ]
})
export class DepartmentModule { }
