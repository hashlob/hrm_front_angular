import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListDepartmentsComponent } from './list-departments/list-departments.component';
import { AddDepartmentComponent } from './add-department/add-department.component';

const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "list",
        component: ListDepartmentsComponent,
        data: {
          title: "Manage Departments"
        }
      },
      {
        path: "add",
        component: AddDepartmentComponent,
        data: {
          title: "Manage Departments"
        }
      },
      {
        path: "add/:id",
        component: AddDepartmentComponent,
        data: {
          title: "Manage Departments"
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartmentRoutingModule { }
