import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceDetailsEditComponent } from './experience-details-edit.component';

describe('ExperienceDetailsEditComponent', () => {
  let component: ExperienceDetailsEditComponent;
  let fixture: ComponentFixture<ExperienceDetailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperienceDetailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceDetailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
