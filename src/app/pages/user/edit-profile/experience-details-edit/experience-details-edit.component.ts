import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-experience-details-edit',
  templateUrl: './experience-details-edit.component.html',
  styleUrls: ['./experience-details-edit.component.scss']
})
export class ExperienceDetailsEditComponent implements OnInit {
  @Input() userId = null;
  @Input() isEdit = false;
  rows = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;

  columns = [
    { name: 'Employeer' },
    { name: 'Position' },
    { name: 'Joining Date' },
    { name: 'Leaving Date' },
    { name: 'Last Salary' },
    { name: 'Reason for Leaving' },
    { name: 'Salary Slip' },
    { name: 'Experience Letter' },
    { name: 'Actions' },
  ];
  salary_slip
  experience_letter
  id

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private userService: UserService,
    private toaster: ToastrService
  ) { }

  experienceForm = this.fb.group({
    employer: [''],
    position: [''],
    joining_date: [''],
    leaving_date: [''],
    last_salary: [''],
    salary_slip: [''],
    experience_letter: [''],
    reason_for_leaving: [''],
  })

  experienceDetails = []
  experienceDetail

  ngOnInit() {
    this.fetchAllExperienceDetails()

  }

  fetchAllExperienceDetails() {
    this.userService.fetchAllExperienceDetails(this.userId)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {

          this.experienceDetails = data
        }
      }).catch(err => {
        console.log(err)
      })
  }

  fetchExperienceDetailById(id) {
    this.userService.fetchExperienceDetailById(id)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {
          this.experienceDetail = data
          this.setForm()
        }
      }).catch(err => {
        console.log(err)
      })
  }

  setForm() {
    this.experienceForm.get("employer").setValue(this.experienceDetail.employer)
    this.experienceForm.get("position").setValue(this.experienceDetail.position)
    this.experienceForm.get("joining_date").setValue(this.experienceDetail.joining_date)
    this.experienceForm.get("leaving_date").setValue(this.experienceDetail.leaving_date)
    this.experienceForm.get("last_salary").setValue(this.experienceDetail.last_salary)
    this.experienceForm.get("reason_for_leaving").setValue(this.experienceDetail.reason_for_leaving)
  }

  open(content, id = null) {
    this.resetForm()
    if (id) {
      this.id = id
      this.fetchExperienceDetailById(id)
    }

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  resetForm() {
    this.experienceForm.reset();
  }

  addExperienceDetails() {

    let formData = new FormData;
    formData.append("employer", this.experienceForm.value.employer)
    formData.append("position", this.experienceForm.value.position)
    formData.append("joining_date", this.experienceForm.value.joining_date)
    formData.append("leaving_date", this.experienceForm.value.leaving_date)
    formData.append("last_salary", this.experienceForm.value.last_salary)
    formData.append("reason_for_leaving", this.experienceForm.value.reason_for_leaving)
    formData.append("salary_slip", this.salary_slip)
    formData.append("experience_letter", this.experience_letter)
    formData.append("user_id", this.userId)

    if (this.id) {
      formData.append("id", this.id)
    }
    this.userService.addExperienceDetail(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.close()
          this.fetchAllExperienceDetails()
          this.toaster.success(message)
          this.id = null
          this.resetForm();
        }
        console.log(data)
      }).catch(err => {
        let { status, error } = err
        if (status == 422) {
          this.toaster.error(error.message)
        }
      })
  }


  processSalarySlip(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if (file) {
        if ((/\.(jpg|jpeg|png|docx|pdf)$/i).test(file.name) && file.size <= 2000000) {
          this.experienceForm.controls['salary_slip'].setErrors(null);
          this.salary_slip = file;
        } else {
          this.experienceForm.controls['salary_slip'].setErrors({ invalid: true });
        }
      }
    }
  }

  processExperienceLetter(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if (file) {
        if ((/\.(jpg|jpeg|png|docx|pdf)$/i).test(file.name) && file.size <= 2000000) {
          this.experienceForm.controls['experience_letter'].setErrors(null);
          this.experience_letter = file;
        } else {
          this.experienceForm.controls['experience_letter'].setErrors({ invalid: true });
        }
      }
    }
  }

}
