import { Component, OnInit, Input } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { UserService } from '../../user.service';
import { SharedService } from 'app/shared/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-documents',
  templateUrl: './user-documents.component.html',
  styleUrls: ['./user-documents.component.scss']
})
export class UserDocumentsComponent implements OnInit {
  @Input() isEdit = false;
  @Input() userId = null;
  constructor(
    private sharedService: SharedService,
    private userService: UserService,
    private fb: FormBuilder,
    private toaster: ToastrService,
    private modalService: NgbModal,
  ) { }

  columns = [
    // { name: 'No' },
    { name: 'Document Type' },
    { name: 'Attachment' },
    { name: 'Actions' },
  ];

  documentDetails = null

  documentForm = this.fb.group({
    document_type_id: ['', [Validators.required]],
  })
  doc_attachment = null
  docTypes = []
  ngOnInit() {

    this.getDocTypes()
    this.getUserDocuments()
  }

  getDocTypes() {
    this.sharedService.getDBVariable({ type_id: AppConfig.DBVariablesId.DocumentType })
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.docTypes = data[0].details
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  getUserDocuments() {
    this.userService.getUserDocuments(this.userId)
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.documentDetails = data;
        }
      }).catch(err => console.log(err))
  }

  open(content, id = null) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  processDoc(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if (file) {
        if ((/\.(jpg|jpeg|png|docx|pdf)$/i).test(file.name) && file.size <= 2000000) {
          // this.documentForm.controls['cnic_attachment'].setErrors(null);
          this.doc_attachment = file;
        } else {
          this.documentForm.controls['cnic_attachment'].setErrors({ invalid: true });
        }
      }
    }
  }

  addDocument() {

    let formData = new FormData;
    formData.append("document_type_id", this.documentForm.value.document_type_id)
    formData.append("attachment", this.doc_attachment)
    formData.append("user_id", this.userId)

    this.userService.addDocument(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.close()
          this.toaster.success(message)
          this.getUserDocuments();
          this.resetForm();
        }
        console.log(data)
      }).catch(err => {
        let { status, error } = err
        if (status == 422) {
          this.toaster.error(error.message)
        }
      })
  }

  resetForm() {
    this.doc_attachment = null
    this.documentForm.reset();
  }

  downloadDoc(attachment, user_id) {
    this.userService.getAttachment({ user_id: this.userId, attachment: attachment })
      .then((res: any) => {
        debugger
        const url = window.URL.createObjectURL(res);
        window.open(url);
      })
  }

}
