import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import AppConfig from 'app/shared/app-config/appConfig';

@Component({
  selector: 'app-personal-details-edit',
  templateUrl: './personal-details-edit.component.html',
  styleUrls: ['./personal-details-edit.component.scss']
})
export class PersonalDetailsEditComponent implements OnInit {
  @Input() isEdit = false;
  @Input() userId = null;

  loading = false;

  constructor(
    private sharedService: SharedService,
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
    private toaster: ToastrService
  ) { }

  userForm = this.fb.group({
    employee_number: ['', [Validators.required, Validators.pattern('^PF-[0-9]+$')]],
    first_name: ['', [Validators.required]],
    last_name: ['', [Validators.required]],
    contact_number: ['', [Validators.required, Validators.pattern('^[0-9]{0,11}$')]],
    contact_number_two: [''],
    email: ['', [Validators.required, Validators.email]],
    cnic: ['', [Validators.required, Validators.pattern('^[0-9]{5}-[0-9]{7}-[0-9]$')]],
    nationality: [''],
    gender: ['', [Validators.required]],
    martial_status: ['', [Validators.required]],
    blood_group: [''],
    date_of_birth: [''],
    city: [''],
    country: [''],
    address: [''],
    username: ['', [Validators.required]],
    role_id: ['', [Validators.required]],
    leave_config_id: ['', [Validators.required]],
    cnic_attachment: ['']
  })

  cnic_attachment = null

  LeaveConfigs = []
  roles = []
  employeeDetails = null
  errors = []
  isFormError = false

  ngOnInit() {
    this.fetchRoles()
    this.fetchLeaveConfigs()
    if (this.isEdit) {
      this.fetchEmployeePersonalDetails()
    } else {
      this.initForm()
    }
  }

  fetchLeaveConfigs() {
    this.sharedService.getDBVariable({ type_id: AppConfig.DBVariablesId.LeaveConfig })
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.LeaveConfigs = data[0].details
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  fetchRoles() {
    this.sharedService.getRolesForSelect({})
      .then((res: any) => {
        let { data, code, message } = res

        if (code == 200) {
          this.roles = data
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  fetchEmployeePersonalDetails() {
    this.userService.fetchEmployeePersonalDetails(this.userId)
      .then((res: any) => {
        let { data, code, message } = res

        if (code == 200) {
          this.employeeDetails = data[0]
          this.initForm()
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  initForm() {
    if (this.isEdit) {
      this.userForm = this.fb.group({
        employee_number: [this.employeeDetails.employee_number, [Validators.required, Validators.pattern('^PF-[0-9]+$')]],
        password: [''],
        first_name: [this.employeeDetails.first_name, [Validators.required]],
        last_name: [this.employeeDetails.last_name, [Validators.required]],
        contact_number: [this.employeeDetails.contact_number, [Validators.required, Validators.pattern('^[0-9]{0,11}$')]],
        contact_number_two: [this.employeeDetails.contact_number_two],
        email: [this.employeeDetails.email, [Validators.required, Validators.email]],
        cnic: [this.employeeDetails.cnic, [Validators.required, Validators.pattern('^[0-9]{5}-[0-9]{7}-[0-9]$')]],
        nationality: [this.employeeDetails.nationality],
        gender: [this.employeeDetails.gender, [Validators.required]],
        martial_status: [this.employeeDetails.martial_status, [Validators.required]],
        blood_group: [this.employeeDetails.blood_group],
        date_of_birth: [this.employeeDetails.date_of_birth],
        city: [this.employeeDetails.city],
        country: [this.employeeDetails.country],
        address: [this.employeeDetails.address],
        username: [this.employeeDetails.system_detail.username, [Validators.required, Validators.pattern('^PF-[0-9]+$')]],
        leave_config_id: [this.employeeDetails.system_detail.leave_config_id],
        cnic_attachment: [],
        role_id: [this.employeeDetails.system_detail.role_id],
      })
    } else {
      this.userForm = this.fb.group({
        employee_number: ['', [Validators.required, Validators.pattern('^PF-[0-9]+$')]],
        password: ['', [Validators.required, Validators.minLength(8)]],
        first_name: ['', [Validators.required]],
        last_name: ['', [Validators.required]],
        contact_number: ['', [Validators.required, Validators.pattern('^[0-9]{0,11}$')]],
        contact_number_two: [''],
        email: ['', [Validators.required, Validators.email]],
        cnic: ['', [Validators.required, Validators.pattern('^[0-9]{5}-[0-9]{7}-[0-9]$')]],
        nationality: [''],
        gender: ['', [Validators.required]],
        martial_status: ['', [Validators.required]],
        blood_group: [''],
        date_of_birth: [''],
        city: [''],
        country: [''],
        address: [''],
        username: ['', [Validators.required, Validators.pattern('^PF-[0-9]+$')]],
        role_id: ['', [Validators.required]],
        leave_config_id: ['', [Validators.required]],
        cnic_attachment: [''],
      })
    }
  }

  getFieldName(fieldName) { return this.userForm.get(fieldName); }

  addPersonalDetails() {

    this.loading = true
    let formData = new FormData;

    formData.append("employee_number", this.userForm.get("employee_number").value)
    formData.append("first_name", this.userForm.get("first_name").value)
    formData.append("last_name", this.userForm.get("last_name").value)
    formData.append("contact_number", this.userForm.get("contact_number").value)
    formData.append("contact_number_two", this.userForm.get("contact_number_two").value)
    formData.append("email", this.userForm.get("email").value)
    formData.append("blood_group", this.userForm.get("blood_group").value)
    formData.append("cnic", this.userForm.get("cnic").value)
    formData.append("nationality", this.userForm.get("nationality").value)
    formData.append("gender", this.userForm.get("gender").value)
    formData.append("martial_status", this.userForm.get("martial_status").value)
    formData.append("date_of_birth", this.userForm.get("date_of_birth").value)
    formData.append("city", this.userForm.get("city").value)
    formData.append("country", this.userForm.get("country").value)
    formData.append("address", this.userForm.get("address").value)
    formData.append("username", this.userForm.get("username").value)
    formData.append("role_id", this.userForm.get("role_id").value)
    formData.append("cnic_attachment", this.cnic_attachment)

    if (this.isEdit) {
      formData.append("user_id", this.userId)

    } else {
      formData.append("password", this.userForm.get("password").value)
    }

    this.userService.createEmployee(formData)
      .then((res: any) => {
        let { data, code, message, user_id } = res
        this.loading = false
        if (code == 200) {
          this.isFormError = false
          this.toaster.success(message)
          this.router.navigate([`/user/${user_id}`])
        }
      }).catch(err => {
        let { status, error } = err
        this.isFormError = true
        if (status == 422) {

          for (var row in error.errors) {
            var objArray = error.errors[row]
            objArray.forEach(element => {
              this.errors.push(element)
            });
          }

          console.log(this.errors)
          this.toaster.error(error.message)
        }
      })

  }

  resetForm() {
    this.userForm.reset()
  }

  processCnic(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if (file) {
        if ((/\.(jpg|jpeg|png|docx|pdf)$/i).test(file.name) && file.size <= 2000000) {
          this.userForm.controls['cnic_attachment'].setErrors(null);
          this.cnic_attachment = file;
        } else {
          this.userForm.controls['cnic_attachment'].setErrors({ invalid: true });
        }
      }
    }
  }

}
