import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HiringDetailsEditComponent } from './hiring-details-edit.component';

describe('HiringDetailsEditComponent', () => {
  let component: HiringDetailsEditComponent;
  let fixture: ComponentFixture<HiringDetailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HiringDetailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HiringDetailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
