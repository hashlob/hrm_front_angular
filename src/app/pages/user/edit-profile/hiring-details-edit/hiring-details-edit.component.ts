import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UserService } from '../../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-hiring-details-edit',
  templateUrl: './hiring-details-edit.component.html',
  styleUrls: ['./hiring-details-edit.component.scss']
})
export class HiringDetailsEditComponent implements OnInit {
  @Input() isEdit = false
  @Input() userId = null
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private toaster: ToastrService
  ) { }

  userForm = this.fb.group({
    joining_date: [''],
    probation_period: [''],
    test_details: [''],
    training_details: [''],
    user_id: ['']
  })

  hiringDetails

  ngOnInit() {
    this.fetchHiringDetails()
  }

  fetchHiringDetails() {
    this.userService.getUsersHiringDetail(this.userId)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.hiringDetails = data[0]
          this.setForm()
        }
      }).catch(err => {
        console.log(err)
      })
  }

  setForm() {
    
    this.userForm = this.fb.group({
      joining_date: [this.hiringDetails.joining_date],
      probation_period: [this.hiringDetails.probation_period],
      test_details: [this.hiringDetails.test_details],
      training_details: [this.hiringDetails.training_details],
    })
  }

  addHiringDetails() {
    let formData = new FormData;

    formData.append("joining_date", this.userForm.get("joining_date").value)
    formData.append("probation_period", this.userForm.get("probation_period").value)
    formData.append("test_details", this.userForm.get("test_details").value)
    formData.append("training_details", this.userForm.get("training_details").value)
    formData.append("user_id", this.userId)

    this.userService.createUsersHiringDetail(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.toaster.success(message)
        }

      }).catch(err => {
        console.log(err)
      })
  }

  resetForm() {
    this.userForm.reset()
  }

}
