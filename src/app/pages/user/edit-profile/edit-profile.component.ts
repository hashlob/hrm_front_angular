import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  isEdit: boolean = false
  userId = null
  titleText = "Create Employee"
  constructor(
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.checkIdParam()
  }

  checkIdParam() {
    this.userId = this.activeRoute.snapshot.paramMap.get("userId")
    if (this.userId) {
      this.isEdit = true
      this.titleText = "Edit Employee"
    } else {
      this.isEdit = false
    }
  }

}
