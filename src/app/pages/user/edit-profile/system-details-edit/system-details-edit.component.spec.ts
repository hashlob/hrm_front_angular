import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemDetailsEditComponent } from './system-details-edit.component';

describe('SystemDetailsEditComponent', () => {
  let component: SystemDetailsEditComponent;
  let fixture: ComponentFixture<SystemDetailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemDetailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemDetailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
