import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../user.service';
import { ToastrService } from 'ngx-toastr';
import { __spread } from 'tslib';
import * as FileSaver from 'file-saver';
@Component({
  selector: 'app-eductaion-details-edit',
  templateUrl: './eductaion-details-edit.component.html',
  styleUrls: ['./eductaion-details-edit.component.scss']
})
export class EductaionDetailsEditComponent implements OnInit {
  @Input() userId = null;
  @Input() isEdit = false;
  rows = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;

  columns = [
    // { name: 'No' },
    { name: 'Qualification' },
    { name: 'Institute' },
    { name: 'Start Date' },
    { name: 'Passing Date' },
    { name: 'Majors' },
    { name: 'Grade' },
    { name: 'Marksheet' },
    { name: 'Degree' },
    { name: 'Actions' },
  ];
  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private userService: UserService,
    private toaster: ToastrService
  ) { }

  educationForm = this.fb.group({
    qualification: [''],
    institute: [''],
    starting_date: [''],
    passing_date: [''],
    majors: [''],
    grade: [''],
    marksheet: ['', [Validators.required]],
    degree: ['', [Validators.required]],
  })

  educationDetails = []

  educationDetail

  marksheet
  degree
  id

  ngOnInit() {
    this.fetchAllEducationDetails()
  }

  fetchAllEducationDetails() {
    this.userService.fetchAllEducationDetails(this.userId)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {

          this.educationDetails = data
        }
      }).catch(err => {
        console.log(err)
      })

  }

  fetchEducationDetailById(id) {
    this.userService.fetchEducationDetailById(id)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {
          this.educationDetail = data
          this.setForm()
        }
      }).catch(err => {
        console.log(err)

      })
  }

  setForm() {
    this.educationForm.get("qualification").setValue(this.educationDetail.qualification)
    this.educationForm.get("institute").setValue(this.educationDetail.institute)
    this.educationForm.get("starting_date").setValue(this.educationDetail.starting_date)
    this.educationForm.get("passing_date").setValue(this.educationDetail.passing_date)
    this.educationForm.get("majors").setValue(this.educationDetail.majors)
    this.educationForm.get("grade").setValue(this.educationDetail.grade)
  }

  open(content, id = null) {
    this.resetForm()
    if (id) {
      this.id = id
      this.fetchEducationDetailById(id)
    }

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  resetForm() {
    this.marksheet = null
    this.degree = null
    this.educationForm.reset();
  }

  addEducationDetails() {

    let formData = new FormData;
    console.log(this.marksheet)
    formData.append("qualification", this.educationForm.value.qualification)
    formData.append("institute", this.educationForm.value.institute)
    formData.append("starting_date", this.educationForm.value.starting_date)
    formData.append("passing_date", this.educationForm.value.passing_date)
    formData.append("majors", this.educationForm.value.majors)
    formData.append("grade", this.educationForm.value.grade)
    formData.append("marksheet", this.marksheet)
    formData.append("degree", this.degree)
    formData.append("user_id", this.userId)

    if (this.id) {
      formData.append("id", this.id)
    }
    this.userService.addEducationDetail(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.close()
          this.fetchAllEducationDetails()
          this.toaster.success(message)
          this.id = null
          this.resetForm();
        }
        console.log(data)
      }).catch(err => {
        let { status, error } = err
        if (status == 422) {
          this.toaster.error(error.message)
        }
      })
  }

  processMarksheet(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if (file) {
        if ((/\.(jpg|jpeg|png|docx|pdf)$/i).test(file.name) && file.size <= 2000000) {
          this.educationForm.controls['marksheet'].setErrors(null);
          this.marksheet = file;
        } else {
          this.educationForm.controls['marksheet'].setErrors({ invalid: true });
        }
      }
    }
  }

  processDegree(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if (file) {
        if ((/\.(jpg|jpeg|png|docx|pdf)$/i).test(file.name) && file.size <= 2000000) {
          this.educationForm.controls['degree'].setErrors(null);
          this.degree = file;
        } else {
          this.educationForm.controls['degree'].setErrors({ invalid: true });
        }
      }
    }
  }

  downloadMarksheet(filename) {
    // this.userService.getMarksheet({ userId: this.userId, filename: filename })

    this.userService.getMarksheet({ userId: this.userId, filename: filename })
      .then((res: any) => {
        debugger
        const url = window.URL.createObjectURL(res);
        window.open(url);
      })
  }

  downloadDegree(filename) {
    this.userService.getDegree({ userId: this.userId, filename: filename })
      .then((res: any) => {
        const url = window.URL.createObjectURL(res);
        window.open(url);
      })
  }

  deleteEductaion(id) {
    let payload = {
      id
    }
    this.userService.deleteEductaion(payload)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          let row = this.rows.filter(x => x.id != id)
          this.educationDetails = row
          this.toaster.success(message)
        }
      }).catch(err => {
        console.log(err)
      })
  }

}
