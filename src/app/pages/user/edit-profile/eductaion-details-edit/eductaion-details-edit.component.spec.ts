import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EductaionDetailsEditComponent } from './eductaion-details-edit.component';

describe('EductaionDetailsEditComponent', () => {
  let component: EductaionDetailsEditComponent;
  let fixture: ComponentFixture<EductaionDetailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EductaionDetailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EductaionDetailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
