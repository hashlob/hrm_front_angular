import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { UserService } from '../../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-emergency-contacts-edit',
  templateUrl: './emergency-contacts-edit.component.html',
  styleUrls: ['./emergency-contacts-edit.component.scss']
})
export class EmergencyContactsEditComponent implements OnInit {

  @Input() userId = null;
  @Input() isEdit = false;
  rows = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  columns = [
    // { name: 'No' },
    { name: 'Name' },
    { name: 'CNIC' },
    { name: 'Contact' },
    { name: 'Relation' },
    { name: 'Address' },
    { name: 'Actions' },
  ];

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private userService: UserService,
    private toaster: ToastrService
  ) { }

  emergencyForm = this.fb.group({
    name: [''],
    cnic: [''],
    contact: [''],
    relation: [''],
    address: [''],
  })

  emergencyDetails = []

  emergencyDetail

  id

  ngOnInit() {
    this.fetchAllEmergencyDetails()

  }

  fetchAllEmergencyDetails() {
    this.userService.fetchAllEmergencyDetails(this.userId)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {

          this.emergencyDetails = data
        }
      }).catch(err => {
        console.log(err)
      })
  }

  fetchEmergencyDetailById(id) {
    this.userService.fetchEmergencyDetailById(id)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {

          this.emergencyDetail = data
          this.setForm()

        }
      }).catch(err => {
        console.log(err)
      })
  }

  setForm() {
    this.emergencyForm.get("name").setValue(this.emergencyDetail.name)
    this.emergencyForm.get("cnic").setValue(this.emergencyDetail.cnic)
    this.emergencyForm.get("contact").setValue(this.emergencyDetail.contact)
    this.emergencyForm.get("relation").setValue(this.emergencyDetail.relation)
    this.emergencyForm.get("address").setValue(this.emergencyDetail.address)
  }

  open(content, id = null) {
    this.resetForm()
    if (id) {
      this.id = id
      this.fetchEmergencyDetailById(id)
    }

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  resetForm() {
    this.emergencyForm.reset();
  }

  addEmergencyDetails() {

    let formData = new FormData;
    formData.append("name", this.emergencyForm.value.name)
    formData.append("cnic", this.emergencyForm.value.cnic)
    formData.append("contact", this.emergencyForm.value.contact)
    formData.append("relation", this.emergencyForm.value.relation)
    formData.append("address", this.emergencyForm.value.address)
    formData.append("user_id", this.userId)

    if (this.id) {
      formData.append("id", this.id)
    }
    this.userService.addEmergencyDetail(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.close()
          this.fetchAllEmergencyDetails()
          this.toaster.success(message)
          this.id = null
          this.resetForm();
        }
        console.log(data)
      }).catch(err => {
        let { status, error } = err
        if (status == 422) {
          this.toaster.error(error.message)
        }
      })
  }

}
