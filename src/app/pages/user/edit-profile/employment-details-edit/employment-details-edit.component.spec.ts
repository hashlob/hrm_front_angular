import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmploymentDetailsEditComponent } from './employment-details-edit.component';

describe('EmploymentDetailsEditComponent', () => {
  let component: EmploymentDetailsEditComponent;
  let fixture: ComponentFixture<EmploymentDetailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmploymentDetailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmploymentDetailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
