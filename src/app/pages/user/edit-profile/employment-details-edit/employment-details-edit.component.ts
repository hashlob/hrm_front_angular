import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../user.service';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from 'app/shared/shared.service';
import AppConfig from 'app/shared/app-config/appConfig';

@Component({
  selector: 'app-employment-details-edit',
  templateUrl: './employment-details-edit.component.html',
  styleUrls: ['./employment-details-edit.component.scss']
})
export class EmploymentDetailsEditComponent implements OnInit {
  @Input() isEdit = false;
  @Input() userId = null;
  loading = false

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private toaster: ToastrService,
    private sharedService: SharedService
  ) { }

  userForm = this.fb.group({
    employee_type_id: [''],
    confirmation_date: [''],
    quit_date: [''],
    salary: ['', [Validators.required]],
    is_tranfer_allow: [''],
    transfer_period: [''],
    grade: [''],
    payment_type: [''],
    designation_id: ['', [Validators.required]],
    unit_id: ['', [Validators.required]],
    department_id: ['', [Validators.required]],
    shift_timein: ['', [Validators.required]],
    shift_timeout: ['', [Validators.required]],
    next_transfer: ['', [Validators.required]],
    user_id: [this.userId]

  })
  employeeDetails = null
  units = []
  departments = []
  designations = []
  paymentTypes = []
  employee_types = []

  ngOnInit() {
    this.fetchUnits()
    this.fetchDepartments()
    this.fetchDesignations()
    this.fetchEmployeeTypes()
    this.getPaymentType()
    if (this.isEdit) {
      this.fetchEmployeementDetails()
    } else {
      this.initForm()
    }
  }

  fetchEmployeeTypes(){
    this.sharedService.getDBVariable({ type_id: AppConfig.DBVariablesId.EmployeeType })
    .then((res: any) => {
      let { data, code, message } = res
      if (code == 200) {
        console.log(data[0].details)
        this.employee_types = data[0].details
      }
    }).catch(err => {
      let { status, error } = err
      console.log(err)
    })
  }

  getPaymentType() {
    this.sharedService.getDBVariable({ type_id: AppConfig.DBVariablesId.PaymentType })
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          console.log(data[0].details)
          this.paymentTypes = data[0].details
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  fetchUnits() {
    this.sharedService.getUnitForSelect({})
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.units = data
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  fetchDepartments() {
    this.sharedService.getDepartmentForSelect({})
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.departments = data
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  fetchDesignations() {
    this.sharedService.getDesignationForSelect({})
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.designations = data
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  initForm() {
    if (this.isEdit) {
      this.userForm = this.fb.group({
        employee_type_id: [this.employeeDetails.employee_type_id],
        confirmation_date: [this.employeeDetails.confirmation_date],
        quit_date: [this.employeeDetails.quit_date],
        salary: [this.employeeDetails.salary, [Validators.required]],
        is_tranfer_allow: [this.employeeDetails.is_tranfer_allow],
        transfer_period: [this.employeeDetails.transfer_period],
        grade: [this.employeeDetails.grade],
        payment_type: [this.employeeDetails.payment_type],
        designation_id: [this.employeeDetails.designation_id, [Validators.required]],
        unit_id: [this.employeeDetails.unit_id, [Validators.required]],
        department_id: [this.employeeDetails.department_id, [Validators.required]],
        shift_timein: [this.employeeDetails.shift_timein, [Validators.required]],
        shift_timeout: [this.employeeDetails.shift_timeout, [Validators.required]],
        next_transfer: [this.employeeDetails.next_transfer, [Validators.required]],
        user_id: [this.userId]

      })
    } else {
      this.userForm = this.fb.group({
        employee_type_id: [''],
        confirmation_date: [''],
        quit_date: [''],
        salary: ['', [Validators.required]],
        is_tranfer_allow: [''],
        transfer_period: [''],
        grade: [''],
        payment_type: [''],
        designation_id: ['', [Validators.required]],
        unit_id: ['', [Validators.required]],
        department_id: ['', [Validators.required]],
        shift_timein: ['', [Validators.required]],
        shift_timeout: ['', [Validators.required]],
        next_transfer: ['', [Validators.required]],
        user_id: [this.userId]
      })
    }
  }

  fetchEmployeementDetails() {
    this.userService.fetchEmployeementDetails(this.userId)
      .then((res: any) => {
        let { data, code, message } = res

        if (code == 200) {
          this.employeeDetails = data[0]
          this.initForm()
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  addEmploymentDetails() {
    let formData = new FormData;
    this.loading = true

    formData.append("employee_type", this.userForm.value)
    formData.append("confirmation_date", this.userForm.value)
    formData.append("quit_date", this.userForm.value)
    formData.append("salary", this.userForm.value)
    formData.append("is_tranfer_allow", this.userForm.value)
    formData.append("transfer_period", this.userForm.value)
    formData.append("department_id", this.userForm.value)
    formData.append("grade", this.userForm.value)
    formData.append("payment_type", this.userForm.value)
    formData.append("designation_id", this.userForm.value)
    formData.append("unit_id", this.userForm.value)
    formData.append("user_id", this.userId)
    formData.append("shift_timein", this.userForm.value)
    formData.append("shift_timeout", this.userForm.value)
    this.userForm.get('user_id').setValue(this.userId)

    this.userService.createEmploymentDetails(this.userForm.value)
      .then((res: any) => {
        let { data, code, message, user_id } = res
        this.loading = false
        if (code == 200) {
          this.toaster.success(message)
          // this.router.navigate([`/user/${user_id}`])
        }
      }).catch(err => {
        let { status, error } = err

        if (status == 422) {
          this.toaster.error(error.message)
        }
      })
  }

  resetForm() {
    this.userForm.reset()
  }

}
