import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { UserService } from '../../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-family-details-edit',
  templateUrl: './family-details-edit.component.html',
  styleUrls: ['./family-details-edit.component.scss']
})
export class FamilyDetailsEditComponent implements OnInit {

  @Input() userId = null;
  @Input() isEdit = false;
  rows = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  columns = [
    // { name: 'No' },
    { name: 'Name' },
    { name: 'CNIC' },
    { name: 'Nationality' },
    { name: 'Profession' },
    { name: 'Contact' },
    { name: 'Relation' },
    { name: 'Address' },
    { name: 'Blood Group' },
    { name: 'Actions' },
  ];
  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private userService: UserService,
    private toaster: ToastrService
  ) { }

  familyForm = this.fb.group({
    name: [''],
    cnic: [''],
    nationality: [''],
    profession: [''],
    contact: [''],
    relation: [''],
    address: [''],
    blood_group: [''],
  })

  familyDetails = []

  familyDetail

  id

  ngOnInit() {
    this.fetchAllFamilyDetails()

  }

  fetchAllFamilyDetails() {
    this.userService.fetchAllFamilyDetails(this.userId)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {

          this.familyDetails = data
        }
      }).catch(err => {
        console.log(err)
      })
  }

  fetchFamilyDetailById(id) {
    this.userService.fetchFamilyDetailById(id)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {

          this.familyDetail = data
          this.setForm()

        }
      }).catch(err => {
        console.log(err)
      })
  }

  setForm() {
    this.familyForm.get("name").setValue(this.familyDetail.name)
    this.familyForm.get("cnic").setValue(this.familyDetail.cnic)
    this.familyForm.get("nationality").setValue(this.familyDetail.nationality)
    this.familyForm.get("profession").setValue(this.familyDetail.profession)
    this.familyForm.get("contact").setValue(this.familyDetail.contact)
    this.familyForm.get("relation").setValue(this.familyDetail.relation)
    this.familyForm.get("address").setValue(this.familyDetail.address)
    this.familyForm.get("blood_group").setValue(this.familyDetail.blood_group)
  }

  open(content, id = null) {
    this.resetForm()
    if (id) {
      this.id = id
      this.fetchFamilyDetailById(id)
    }

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  resetForm() {
    this.familyForm.reset();
  }

  addFamilyDetails() {

    let formData = new FormData;
    formData.append("name", this.familyForm.value.name)
    formData.append("cnic", this.familyForm.value.cnic)
    formData.append("nationality", this.familyForm.value.nationality)
    formData.append("profession", this.familyForm.value.profession)
    formData.append("contact", this.familyForm.value.contact)
    formData.append("relation", this.familyForm.value.relation)
    formData.append("address", this.familyForm.value.address)
    formData.append("blood_group", this.familyForm.value.blood_group)
    formData.append("user_id", this.userId)

    if (this.id) {
      formData.append("id", this.id)
    }
    this.userService.addFamilyDetail(formData)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.close()
          this.fetchAllFamilyDetails()
          this.toaster.success(message)
          this.id = null
          this.resetForm();
        }
        console.log(data)
      }).catch(err => {
        let { status, error } = err
        if (status == 422) {
          this.toaster.error(error.message)
        }
      })
  }


}
