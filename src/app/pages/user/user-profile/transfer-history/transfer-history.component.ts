import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-transfer-history',
  templateUrl: './transfer-history.component.html',
  styleUrls: ['./transfer-history.component.scss']
})
export class TransferHistoryComponent implements OnInit {
  @Input() userId = null
  rows = [];
  columns = [
    { name: 'No' },
    { name: 'From' },
    { name: 'To' },
    { name: 'Name' },
    { name: 'Date' },

    { name: 'Status' },

  ];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.fetchAllTranferHistory()
  }
  fetchAllTranferHistory() {
    this.userService.fetchTransferList(this.userId).then((res: any) => {
      this.rows = res.data
    }).catch(err => { })
  }

}
