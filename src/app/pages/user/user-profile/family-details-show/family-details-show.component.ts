import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-family-details-show',
  templateUrl: './family-details-show.component.html',
  styleUrls: ['./family-details-show.component.scss']
})
export class FamilyDetailsShowComponent implements OnInit {
@Input() userId =null

allfamilymembers
  constructor(private userService : UserService) { }

  ngOnInit() {
    this.fetchallfamilymembers()
  }
fetchallfamilymembers(){
  this.userService.fetchAllFamilyDetails(this.userId).then((res : any)=>{
    if(res.code == 200){
      debugger
      this.allfamilymembers = res.data[0]
    }
  })
}
}
