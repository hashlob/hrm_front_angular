import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyDetailsShowComponent } from './family-details-show.component';

describe('FamilyDetailsShowComponent', () => {
  let component: FamilyDetailsShowComponent;
  let fixture: ComponentFixture<FamilyDetailsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyDetailsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyDetailsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
