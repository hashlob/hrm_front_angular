import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceDetailsShowComponent } from './experience-details-show.component';

describe('ExperienceDetailsShowComponent', () => {
  let component: ExperienceDetailsShowComponent;
  let fixture: ComponentFixture<ExperienceDetailsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperienceDetailsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceDetailsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
