import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-experience-details-show',
  templateUrl: './experience-details-show.component.html',
  styleUrls: ['./experience-details-show.component.scss']
})
export class ExperienceDetailsShowComponent implements OnInit {
@Input() userId = null
experienceDetail
  constructor(private user : UserService) { }

  ngOnInit() {
this.fetchexperiencedetail()
  }
fetchexperiencedetail(){
  this.user.fetchAllExperienceDetails(this.userId).then((res : any)=>{
    if(res.code == 200){
      this.experienceDetail = res.data[0]
    }
  })
}
}
