import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-emergency-details-show',
  templateUrl: './emergency-details-show.component.html',
  styleUrls: ['./emergency-details-show.component.scss']
})
export class EmergencyDetailsShowComponent implements OnInit {
@Input() userId = null

emergencyDetail
  constructor(private user : UserService) { }

  ngOnInit() {
    this.fetchemergencydetail()
  }
fetchemergencydetail(){
  this.user.fetchAllEmergencyDetails(this.userId).then((res :any)=>{
    if(res.code == 200){
      debugger
      this.emergencyDetail = res.data[0]
    }
  })
}
}
