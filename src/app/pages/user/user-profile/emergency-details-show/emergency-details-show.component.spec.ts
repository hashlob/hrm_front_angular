import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyDetailsShowComponent } from './emergency-details-show.component';

describe('EmergencyDetailsShowComponent', () => {
  let component: EmergencyDetailsShowComponent;
  let fixture: ComponentFixture<EmergencyDetailsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmergencyDetailsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyDetailsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
