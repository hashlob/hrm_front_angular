import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemDetailsShowComponent } from './system-details-show.component';

describe('SystemDetailsShowComponent', () => {
  let component: SystemDetailsShowComponent;
  let fixture: ComponentFixture<SystemDetailsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemDetailsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemDetailsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
