import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HiringDetailsShowComponent } from './hiring-details-show.component';

describe('HiringDetailsShowComponent', () => {
  let component: HiringDetailsShowComponent;
  let fixture: ComponentFixture<HiringDetailsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HiringDetailsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HiringDetailsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
