import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-hiring-details-show',
  templateUrl: './hiring-details-show.component.html',
  styleUrls: ['./hiring-details-show.component.scss']
})
export class HiringDetailsShowComponent implements OnInit {
@Input() userId = null
employeeHiringdetail
  constructor(private userservice : UserService) { }

  ngOnInit() {
    this.fetchemployeehiringdetail()
  }
fetchemployeehiringdetail(){
  debugger
  this.userservice.getUsersHiringDetail(this.userId).then((res : any)=> {
    if(res.code == 200){
      debugger
      this.employeeHiringdetail =res.data[0]
    }
  })
}
}
