import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-education-details-show',
  templateUrl: './education-details-show.component.html',
  styleUrls: ['./education-details-show.component.scss']
})
export class EducationDetailsShowComponent implements OnInit {
@Input() userId = null
  constructor(private userService :UserService) { }
employeeEducationDetail
  ngOnInit() {
    debugger
    this.fetcheducationdetail()
  }
fetcheducationdetail(){
  this.userService.fetchAllEducationDetails(this.userId).then((res : any)=> {
    if(res.code == 200){
      debugger
      this.employeeEducationDetail = res.data[0]
    }
  })
}
}
