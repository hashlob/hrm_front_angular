import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationDetailsShowComponent } from './education-details-show.component';

describe('EducationDetailsShowComponent', () => {
  let component: EducationDetailsShowComponent;
  let fixture: ComponentFixture<EducationDetailsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationDetailsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationDetailsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
