import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-personal-details-show',
  templateUrl: './personal-details-show.component.html',
  styleUrls: ['./personal-details-show.component.scss']
})
export class PersonalDetailsShowComponent implements OnInit {
  @Input() userId = null

  employeeDetails
  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.fetchEmployeePersonalDetails()
  }

  fetchEmployeePersonalDetails() {
    this.userService.fetchEmployeePersonalDetails(this.userId)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.employeeDetails = data[0]
        }

      }).catch(err => {
        console.log(err)
      })

  }

}
