import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalDetailsShowComponent } from './personal-details-show.component';

describe('PersonalDetailsShowComponent', () => {
  let component: PersonalDetailsShowComponent;
  let fixture: ComponentFixture<PersonalDetailsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalDetailsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalDetailsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
