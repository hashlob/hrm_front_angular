import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-employment-details-show',
  templateUrl: './employment-details-show.component.html',
  styleUrls: ['./employment-details-show.component.scss']
})
export class EmploymentDetailsShowComponent implements OnInit {
  @Input() userId = null
  
employmentDetail 
  constructor(private user : UserService) { }

  ngOnInit() {
    this.fetchemplyedetail()
  }
fetchemplyedetail(){
  this.user.fetchEmployeementDetails(this.userId).then((res : any)=>{
    if(res.code == 200) {
      debugger
      this. employmentDetail = res.data[0]
    }
    
  })

}
}
