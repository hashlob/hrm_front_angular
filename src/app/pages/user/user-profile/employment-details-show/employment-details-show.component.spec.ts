import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmploymentDetailsShowComponent } from './employment-details-show.component';

describe('EmploymentDetailsShowComponent', () => {
  let component: EmploymentDetailsShowComponent;
  let fixture: ComponentFixture<EmploymentDetailsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmploymentDetailsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmploymentDetailsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
