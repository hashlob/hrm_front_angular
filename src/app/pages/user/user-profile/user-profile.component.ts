import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  userId: any;

  constructor(
    private activeRoute : ActivatedRoute
  ) { }

  ngOnInit() {
    this.checkIdParam()
  }

  checkIdParam() {
    this.userId = this.activeRoute.snapshot.paramMap.get("userId")
  }

}
