import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProbationListComponent } from './probation-list.component';

describe('ProbationListComponent', () => {
  let component: ProbationListComponent;
  let fixture: ComponentFixture<ProbationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProbationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProbationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
