import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-probation-list',
  templateUrl: './probation-list.component.html',
  styleUrls: ['./probation-list.component.scss']
})
export class ProbationListComponent implements OnInit {

  rows = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;

  columns = [
    { name: 'No' },
    { name: 'Employee Number' },
    { name: 'Name' },
    { name: 'Username' },
    { name: 'Phone' },
    { name: 'E-mail' },
  ];

  constructor(
    private userService: UserService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.probatiobList()

  }

  probatiobList() {
    this.userService.getProbationList()
      .then((res: any) => {
        this.rows = res.data
      }).catch(err => console.log(err))
  }

}
