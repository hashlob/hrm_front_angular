import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { NgbTabsetModule, NgbModalModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { PersonalDetailsShowComponent } from './user-profile/personal-details-show/personal-details-show.component';
import { EmploymentDetailsShowComponent } from './user-profile/employment-details-show/employment-details-show.component';
import { HiringDetailsShowComponent } from './user-profile/hiring-details-show/hiring-details-show.component';
import { SystemDetailsShowComponent } from './user-profile/system-details-show/system-details-show.component';
import { EducationDetailsShowComponent } from './user-profile/education-details-show/education-details-show.component';
import { ExperienceDetailsShowComponent } from './user-profile/experience-details-show/experience-details-show.component';
import { FamilyDetailsShowComponent } from './user-profile/family-details-show/family-details-show.component';
import { EmergencyDetailsShowComponent } from './user-profile/emergency-details-show/emergency-details-show.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { PersonalDetailsEditComponent } from './edit-profile/personal-details-edit/personal-details-edit.component';
import { EmploymentDetailsEditComponent } from './edit-profile/employment-details-edit/employment-details-edit.component';
import { HiringDetailsEditComponent } from './edit-profile/hiring-details-edit/hiring-details-edit.component';
import { SystemDetailsEditComponent } from './edit-profile/system-details-edit/system-details-edit.component';
import { EductaionDetailsEditComponent } from './edit-profile/eductaion-details-edit/eductaion-details-edit.component';
import { ExperienceDetailsEditComponent } from './edit-profile/experience-details-edit/experience-details-edit.component';
import { FamilyDetailsEditComponent } from './edit-profile/family-details-edit/family-details-edit.component';
import { EmergencyContactsEditComponent } from './edit-profile/emergency-contacts-edit/emergency-contacts-edit.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UserListComponent } from './user-list/user-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransferHistoryComponent } from './user-profile/transfer-history/transfer-history.component';
import { UserDocumentsComponent } from './edit-profile/user-documents/user-documents.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ProbationListComponent } from './probation-list/probation-list.component';

@NgModule({
  declarations: [UserProfileComponent, PersonalDetailsShowComponent, EmploymentDetailsShowComponent, HiringDetailsShowComponent, SystemDetailsShowComponent, EducationDetailsShowComponent, ExperienceDetailsShowComponent, FamilyDetailsShowComponent, EmergencyDetailsShowComponent, EditProfileComponent, PersonalDetailsEditComponent, EmploymentDetailsEditComponent, HiringDetailsEditComponent, SystemDetailsEditComponent, EductaionDetailsEditComponent, ExperienceDetailsEditComponent, FamilyDetailsEditComponent, EmergencyContactsEditComponent, UserListComponent, TransferHistoryComponent, UserDocumentsComponent, ProbationListComponent],
  imports: [
    CommonModule,
    NgbTabsetModule,
    UserRoutingModule,
    FormsModule,
    NgbDropdownModule,
    NgSelectModule,
    ReactiveFormsModule,
    NgbModalModule,
    NgxDatatableModule
  ]
})
export class UserModule { }
