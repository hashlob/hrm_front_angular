import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { UserListComponent } from './user-list/user-list.component';
import { ProbationListComponent } from './probation-list/probation-list.component';

const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "probation",
        component: ProbationListComponent,
        data: {
          title: "Probation List"
        },
        
      },
      {
        path: "profile/:userId",
        component: UserProfileComponent,
        data: {
          title: "User Profile"
        }
      },
      {
        path: "list",
        component: UserListComponent,
        data: {
          title: "User List"
        }
      },
      {
        path: ":userId",
        component: EditProfileComponent,
        data: {
          title: "User Edit"
        }
      },
      {
        path: "",
        component: EditProfileComponent,
        data: {
          title: "User Add"
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
