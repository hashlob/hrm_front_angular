import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = AppConfig.API_ENDPOINT + "/";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  getEmployeesList(params) {
    return this.http.get(`${this.baseUrl}user`, {
      headers: this.headers,
      params: params
    }).toPromise()
  }

  fetchEmployeePersonalDetails(userId) {
    return this.http.get(`${this.baseUrl}user/${userId}`, {
      headers: this.headers,
    }).toPromise()
  }

  getProbationList() {
    return this.http.get(`${this.baseUrl}user/getProbationList`, {
      headers: this.headers,
    }).toPromise()
  }

  createEmployee(payload) {
    return this.http.post(`${this.baseUrl}user`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  changeUserStatus(payload) {
    return this.http.post(`${this.baseUrl}user/changeUserStatus`, payload, {
      headers: this.headers,
    }).toPromise()
  }
  updateEmployeePersonalDetails(payload) {
    return this.http.post(`${this.baseUrl}user/update`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  createEmploymentDetails(payload) {
    return this.http.post(`${this.baseUrl}employment`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  fetchEmployeementDetails(userId) {
    return this.http.get(`${this.baseUrl}employment/${userId}`, {
      headers: this.headers,
    }).toPromise()
  }

  createUsersHiringDetail(payload) {
    return this.http.post(`${this.baseUrl}hiring`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  getUsersHiringDetail(userId) {
    return this.http.get(`${this.baseUrl}hiring/${userId}`, {
      headers: this.headers,
    }).toPromise()
  }

  addEducationDetail(payload) {
    return this.http.post(`${this.baseUrl}education`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  deleteEductaion(payload) {
    return this.http.post(`${this.baseUrl}education/delete`, payload, {
      headers: this.headers
    }).toPromise()
  }

  fetchAllEducationDetails(userId) {
    return this.http.get(`${this.baseUrl}education/${userId}`, {
      headers: this.headers,
    }).toPromise()
  }

  fetchEducationDetailById(id) {
    return this.http.get(`${this.baseUrl}education/fetchEducationDetailById/${id}`, {
      headers: this.headers,
    }).toPromise()
  }

  getMarksheet(params) {
    // return `${this.baseUrl}education/getMarksheet?userId=${params.userId}&filename=${params.filename}`
    return this.http.get(`${this.baseUrl}education/getMarksheet`, {
      headers: this.headers,
      responseType: 'blob',
      params: params
    }).toPromise()
  }

  getDegree(params) {
    return this.http.get(`${this.baseUrl}education/getDegree`, {
      headers: this.headers,
      responseType: 'blob',
      params: params
    }).toPromise()
  }

  addExperienceDetail(payload) {
    return this.http.post(`${this.baseUrl}experience`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  fetchAllExperienceDetails(userId) {
    return this.http.get(`${this.baseUrl}experience/${userId}`, {
      headers: this.headers,
    }).toPromise()
  }

  fetchExperienceDetailById(id) {
    return this.http.get(`${this.baseUrl}experience/fetchExperienceDetailById/${id}`, {
      headers: this.headers,
    }).toPromise()
  }

  addFamilyDetail(payload) {
    return this.http.post(`${this.baseUrl}family`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  fetchAllFamilyDetails(userId) {
    return this.http.get(`${this.baseUrl}family/${userId}`, {
      headers: this.headers,
    }).toPromise()
  }

  fetchFamilyDetailById(id) {
    return this.http.get(`${this.baseUrl}family/getFamilyDetailById/${id}`, {
      headers: this.headers,
    }).toPromise()
  }

  addEmergencyDetail(payload) {
    return this.http.post(`${this.baseUrl}emergencyContacts`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  fetchAllEmergencyDetails(userId) {
    return this.http.get(`${this.baseUrl}emergencyContacts/${userId}`, {
      headers: this.headers,
    }).toPromise()
  }

  fetchEmergencyDetailById(id) {
    return this.http.get(`${this.baseUrl}emergencyContacts/fetchEmergencyDetailById/${id}`, {
      headers: this.headers,
    }).toPromise()
  }

  fetchTransferList(params) {
    return this.http.get(`${this.baseUrl}transferRequests/getTransferRequests`, {

      headers: this.headers,
      params
    }).toPromise()
  }

  addDocument(payload) {
    return this.http.post(`${this.baseUrl}document`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  getUserDocuments(id) {
    return this.http.get(`${this.baseUrl}document/${id}`, {
      headers: this.headers,
    }).toPromise()
  }

  getAttachment(params) {
    return this.http.get(`${this.baseUrl}document/getAttachment`, {
      headers: this.headers,
      responseType: 'blob',
      params: params
    }).toPromise()
  }

}
