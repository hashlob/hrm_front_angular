import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import AppConfig from 'app/shared/app-config/appConfig';
import { SharedService } from 'app/shared/shared.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  rows = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;

  columns = [
    { name: 'No' },
    { name: 'Employee Number' },
    { name: 'Name' },
    { name: 'Username' },
    { name: 'Phone' },
    { name: 'E-mail' },
    { name: 'Actions' },
  ];

  genderTypes = [
    { id: 'male', name: 'Male' },
    { id: 'female', name: 'Female' },
  ]
  statusType = [
    { id: 1, name: "Active" },
    { id: 0, name: "Inactive" }
  ]
  employeeType = []
  departments = []
  units = []
  designations = []

  constructor(
    private userService: UserService,
    private router: Router,
    private toaster: ToastrService,
    private sharedService: SharedService
  ) { }

  searchOption = {}

  ngOnInit() {
    this.fetchUnits()
    this.fetchDepartments()
    this.fetchDesignations()
    this.fetchEmployeeTypes()
    this.fetchDepartments()
    this.fetchEmployeeList(this.searchOption)
  }

  search() {
    this.fetchEmployeeList(this.searchOption)
  }

  resetSearch() {
    this.searchOption = {}
    this.fetchEmployeeList(this.searchOption)

  }

  fetchEmployeeTypes() {
    this.sharedService.getDBVariable({ type_id: AppConfig.DBVariablesId.EmployeeType })
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          console.log(data[0].details)
          this.employeeType = data[0].details
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  fetchUnits() {
    this.sharedService.getUnitForSelect({})
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.units = data
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  fetchDepartments() {
    this.sharedService.getDepartmentForSelect({})
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.departments = data
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  fetchDesignations() {
    this.sharedService.getDesignationForSelect({})
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.designations = data
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  fetchEmployeeList(params) {
    this.userService.getEmployeesList(params)
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.rows = data
        }
      }).catch(err => {
        let { status, error } = err
        console.log(err)
      })
  }

  gotoProfile(id) {
    this.router.navigate([`/user/profile/${id}`])
  }

  gotoEditProfile(id) {
    this.router.navigate([`/user/${id}`])
  }

  getCounts(i) {
    return i + 1;
  }

  changeStatus(id, status) {
    let payload = {
      userId: id,
      status: status
    }

    this.userService.changeUserStatus(payload)
      .then((res: any) => {
        let { code, message, data, user_id } = res

        if (code == 200) {
          let findUserIndex = this.rows.findIndex(x => x.id == user_id)
          let findUser = this.rows[findUserIndex]

          findUser.is_active = status
          this.rows[findUserIndex] = findUser
          this.toaster.success(message)
        }
      }).catch(err => {
        console.log(err)
      })
  }
}
