import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AllowanceService {

  baseUrl = AppConfig.API_ENDPOINT + "/allowance";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  addUserAllowance(payload) {
    return this.http.post(`${this.baseUrl}/addUserAllowance`, payload, {
      headers: this.headers
    }).toPromise()
  }

  getUserAllowances(params) {
    return this.http.get(`${this.baseUrl}/getUserAllowances`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  deleteAllowance(id) {
    return this.http.get(`${this.baseUrl}/delete/${id}`, {
      headers: this.headers
    }).toPromise()
  }
}
