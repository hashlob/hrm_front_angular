import { TestBed } from '@angular/core/testing';

import { AllowanceService } from './allowance.service';

describe('AllowanceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AllowanceService = TestBed.get(AllowanceService);
    expect(service).toBeTruthy();
  });
});
