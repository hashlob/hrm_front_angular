import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListAllowanceComponent } from './list-allowance/list-allowance.component';

const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: 'list',
        component: ListAllowanceComponent,
        data: {
          title: "User Allowances"
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllowanceRoutingModule { }
