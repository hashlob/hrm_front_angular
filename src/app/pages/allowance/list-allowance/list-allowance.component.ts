import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import AppConfig from 'app/shared/app-config/appConfig';
import { AllowanceService } from '../allowance.service';
import * as moment from "moment"

@Component({
  selector: 'app-list-allowance',
  templateUrl: './list-allowance.component.html',
  styleUrls: ['./list-allowance.component.scss']
})
export class ListAllowanceComponent implements OnInit {

  constructor(
    private sharedService: SharedService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private allowanceService: AllowanceService,
    private toaster: ToastrService
  ) { }
  valueType = [
    { name: 'Percent', id: 'Percent' },
    { name: 'Amount', id: 'Amount' }
  ]
  allowanceForm = this.fb.group({
    user_id: ['', [Validators.required]],
    value: ['', [Validators.required]],
    remarks: [''],
    allowance_type_id: ['', [Validators.required]],
    value_type: ['', [Validators.required]],
    is_monthly: [''],
    applied_on: ['']
  })
  columns = [
    { name: 'No' },
    { name: 'Allowance' },
    { name: 'User' },
    { name: 'Value' },
    { name: 'Remarks' },
    { name: 'Actions' }
  ];
  allowanceDetails = []
  users = []
  allowanceTypes = []

  ngOnInit() {
    this.fetchUsersForDropDown()
    this.fetchAllowanceTypes()
    this.getUserAllowances()
  }

  getUserAllowances(params = {}) {
    this.allowanceService.getUserAllowances(params)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.allowanceDetails = data
        }
      }).catch(err => {
        console.log(err)
      })
  }

  fetchAllowanceTypes() {
    this.sharedService.getDBVariable({ type_id: AppConfig.DBVariablesId.Allowance })
      .then((res: any) => {
        let { code, messsage, data } = res
        if (code == 200) {
          this.allowanceTypes = data[0].details.map(x => {
            return {
              id: x.id,
              name: x.options
            }
          })
        }
      }).catch(err => {
        console.log(err)
      })
  }

  fetchUsersForDropDown() {
    this.sharedService.getUsersForSelect({ all: true })
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.users = data.map(x => {
            return {
              id: x.id,
              name: x.first_name + " " + x.last_name
            }
          })
        }
      }).catch(err => {
        console.log(err)
      })
  }

  open(content, id = null) {
    this.allowanceForm.reset()
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  addAllowance() {
    let date = moment().format("YYYY-MM-DD")
    this.allowanceForm.get('applied_on').setValue(date)
    this.allowanceForm.value
    this.allowanceService.addUserAllowance(this.allowanceForm.value)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.close()
          this.getUserAllowances()
          this.toaster.success(message)
          this.allowanceForm.reset()
        }
      }).catch(err => {
        console.log(err)
      })
  }

  delete(id) {
    this.allowanceService.deleteAllowance(id)
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.toaster.success(message)
          let filtered = this.allowanceDetails.filter(x => x.id != id)
          this.allowanceDetails = filtered
        }
      }).catch(err => {
        console.log(err)
      })
  }
}
