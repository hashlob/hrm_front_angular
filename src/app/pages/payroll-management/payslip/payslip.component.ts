import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PayrollService } from '../payroll.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import * as moment from 'moment';
import AppConfig from 'app/shared/app-config/appConfig';

@Component({
  selector: 'app-payslip',
  templateUrl: './payslip.component.html',
  styleUrls: ['./payslip.component.scss']
})
export class PayslipComponent implements OnInit {

  @ViewChild('pdfarea') pdfTable: ElementRef;
  payroll
  bonuses = []
  deductions = []
  fines = []
  user_id 
  month
  year
  selectedMonth = ''

  constructor(
    private payrollService: PayrollService
  ) { }

  ngOnInit() {
    let date = new Date;
    this.user_id = AppConfig.getUserId();
    this.setMonth(date);
  }

  monthChange(e) {
    this.setMonth(e.target.value)
  }

  setMonth(date) {
    this.month = moment(date).format("MM")
    this.year = moment(date).format("YYYY")
    this.selectedMonth = moment(date).format("YYYY-MM")
    this.fetchPayslip()
  }


  fetchPayslip() {
    let params = {
      month: this.month,
      user_id: this.user_id,
      year: this.year
    }
    this.payrollService.getPayslip(params)
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.payroll = data.payroll
          this.bonuses = data.bonuses
          this.deductions = data.deductions
          this.fines = data.fines
        }
      }).catch(err => console.log(err))
  }

  downloadPdf() {
    //   const doc = new jsPDF('p', 'mm', 'a4');

    //   const specialElementHandlers = {
    //     '#editor': function (element, renderer) {
    //       return true;
    //     }
    //   };

    //   const pdfTable = this.pdfTable.nativeElement;

    //   doc.fromHTML(pdfTable, 15, 15, {
    //     width: 300,
    //     elementHandlers: specialElementHandlers
    // });

    //   doc.save('tableToPdf.pdf');

    var data = document.getElementById('pdfarea');  //Id of the table
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      let imgWidth = 208;
      let pageHeight = 295;
      let imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      let position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('MYPdf.pdf'); // Generated PDF   
    });
  }

  // print() {
  //   let printContents, popupWin;
  //   printContents = document.getElementById('print-section').innerHTML;
  //   popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
  //   popupWin.document.open();
  //   popupWin.document.write(`
  //     <html>
  //       <head>
  //         <title>Print tab</title>
  //         <style>
  //         //........Customized style.......
  //         </style>
  //       </head>
  //   <body onload="window.print();window.close()">${printContents}</body>
  //     </html>`
  //   );
  //   popupWin.document.close();
  // }

}
