import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { PayrollService } from '../../payroll.service';

@Component({
  selector: 'app-list-salaries',
  templateUrl: './list-salaries.component.html',
  styleUrls: ['./list-salaries.component.scss']
})
export class ListSalariesComponent implements OnInit {

  constructor(private payrollService: PayrollService,
    private toaster: ToastrService) { }

  ngOnInit() {
    this.fetchAllEmployeeSalaries()

  }
  employeeDetail = []
  columns = [
    { name: 'No' },
    { name: 'Name' },
    { name: 'Department' },
    { name: 'Unit' },
    { name: 'Email' },
    { name: 'Phone' },
    { name: 'Salary' },

  ];
  fetchAllEmployeeSalaries(){
    this.payrollService.getAllEmployeeSalaries().then((res : any)=>{
        this.employeeDetail = res.data
    }).catch(err=>{})
  }
}
