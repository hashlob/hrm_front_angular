import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PayrollService } from '../../payroll.service';
import * as moment from 'moment';

@Component({
  selector: 'app-mark-increment',
  templateUrl: './mark-increment.component.html',
  styleUrls: ['./mark-increment.component.scss']
})
export class MarkIncrementComponent implements OnInit {

  constructor(private sharedServices: SharedService,
    private fb: FormBuilder,
    private router: Router,
    private toaster: ToastrService,
    private payrollService: PayrollService,) { }

  ngOnInit() {
    this.fetchAllUsers()
  }
  users : []
  date =new Date
  incrementForm = this.fb.group({
    user_id :['',Validators.required],
    amount:['',Validators.required],
    date :moment(this.date).format('YYYY-MM-DD')
  })
  fetchAllUsers() {
    this.sharedServices.getUsersForSelect({ all: true }).then((res: any) => {
      let { code, message, data } = res
      this.users = data.map(x => {
        return {
          id: x.id,
          name: x.first_name + ' ' + x.last_name
        }
      })
    })
  }
  addIncrement(){
    this.payrollService.addIncreement(this.incrementForm.value).then((res:any)=>{
let {code , data, message} = res
if(code == 200){
  this.toaster.success(message)
}
    }).catch(err=>{
      this.toaster.error(err)

    })
  }
}
