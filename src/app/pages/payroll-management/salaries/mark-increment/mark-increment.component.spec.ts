import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkIncrementComponent } from './mark-increment.component';

describe('MarkIncrementComponent', () => {
  let component: MarkIncrementComponent;
  let fixture: ComponentFixture<MarkIncrementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkIncrementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkIncrementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
