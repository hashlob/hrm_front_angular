import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PayrollService } from '../payroll.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import AppConfig from 'app/shared/app-config/appConfig';
import * as moment from "moment"

@Component({
  selector: 'app-deduction',
  templateUrl: './deduction.component.html',
  styleUrls: ['./deduction.component.scss']
})
export class DeductionComponent implements OnInit {

  constructor(private sharedServices: SharedService,
    private fb: FormBuilder,
    private router: Router,
    private toaster: ToastrService,
    private payrollService: PayrollService,
    private modalService: NgbModal, ) { }
  columns = [
    { name: 'No' },
    { name: 'Amount' },
    { name: 'Deduction Type' },
    { name: 'Name' },
    { name: 'Month' },
    { name: 'Description' },
    { name: 'Actions' },

  ];
  row = [];
  deductionDetail = []
  deductionType = []
  users = []
  deductions = null
  ngOnInit() {
    this.fetchDeductionType()
    this.fetchAllUsers()
    this.fetchAllDeductionList()
  }


  deductionForm = this.fb.group({
    user_id: ['', Validators.required],
    type_id: ['', Validators.required],
    amount: ['', Validators.required],
    description: ['', Validators.required],
    month: ['',Validators.required],
    id: ''
  })

  fetchDeductionType() {
    this.sharedServices.getDBVariable({ type_id: AppConfig.DBVariablesId.DeductionType }).then((res: any) => {
      let { code, message, data } = res
      if (code == 200) {
        this.deductionType = data[0].details.map(x => {
          return {
            id: x.id,
            name: x.options
          }
        })
      }
    }).catch(err => {

    })
  }

  fetchAllDeductionList() {
    this.payrollService.getAllDeductionList({}).then((res: any) => {
      this.deductionDetail = res.data
      this.deductions = res.data
    }).catch(err => {

    })
  }

  fetchAllUsers() {
    this.sharedServices.getUsersForSelect({ all: true }).then((res: any) => {
      let { code, message, data } = res
      this.users = data.map(x => {
        return {
          id: x.id,
          name: x.first_name + ' ' + x.last_name
        }
      })
    })
  }
  open(content, id = null) {
    debugger
    // this.allowanceForm.reset()
    this.deductionForm.reset()
    if (id) {

      this.fetchDeductionById(id)
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }


  addDeductionForUser() {
    this.payrollService.addDeduction(this.deductionForm.value).then((res: any) => {
      debugger
      let { code, message } = res
      if (code == 200) {
        this.toaster.success(message)
        this.close()
        this.fetchAllDeductionList()

      }

    }).catch(err => {

    })
  }


  setForm() {
    debugger
    this.deductionForm.get("user_id").setValue(this.deductions.user_id)
    this.deductionForm.get("type_id").setValue(this.deductions.type_id)
    this.deductionForm.get("amount").setValue(this.deductions.amount)
    this.deductionForm.get("description").setValue(this.deductions.description)
   this.deductionForm.get("month").setValue(this.deductions.year+'-'+this.deductions.month)
    this.deductionForm.get("id").setValue(this.deductions.id)
  }

  fetchDeductionById(id) {
    this.payrollService.getDeductionById(id).then((res: any) => {
        if (id) {
          let { code, message, data } = res
          if (code == 200) {
            this.deductions = data
            this.setForm()
          }
        }
      })
    }
    delete(id){
      this.payrollService.deleteDeductionById(id).then((res :any)=>{
        debugger
        let {code , message } = res
        if(code == 200) {
          this.toaster.success(message)
          this.fetchAllDeductionList()
        }
      })
    }

    getMonthYear(month, year) {
      let date = `${year}-${month}`
  
      return moment(date,'YYYY-MM').format("MMMM YYYY")
    }
}
