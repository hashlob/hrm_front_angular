import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PayrollService } from '../payroll.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedService } from 'app/shared/shared.service';
import AppConfig from 'app/shared/app-config/appConfig';
import * as moment from "moment"

@Component({
  selector: 'app-fine',
  templateUrl: './fine.component.html',
  styleUrls: ['./fine.component.scss']
})
export class FineComponent implements OnInit {

  constructor(private sharedServices: SharedService,
    private fb: FormBuilder,
    private router: Router,
    private toaster: ToastrService,
    private payrollService: PayrollService,
    private modalService: NgbModal, ) { }
  columns = [
    { name: 'No' },
    { name: 'Amount' },
    { name: 'Fine Type' },
    { name: 'Name' },
    { name: 'Month' },
    { name: 'Description' },
    { name: 'Actions' },

  ];
  row = [];
  fineDetail = []
  fineType = []
  users = []
  fines = null
  ngOnInit() {
    this.fetchFineType()
    this.fetchAllUsers()
    this.fetchAllFineList()
  }
  fineForm = this.fb.group({
    user_id: ['', Validators.required],
    type_id: ['', Validators.required],
    amount: ['', Validators.required],
    description: ['', Validators.required],
    month: ['',Validators.required],
    id: ''
  })
  fetchFineType() {
    this.sharedServices.getDBVariable({ type_id: AppConfig.DBVariablesId.FineType }).then((res: any) => {
      let { code, message, data } = res
      if (code == 200) {
        this.fineType = data[0].details.map(x => {
          return {
            id: x.id,
            name: x.options
          }
        })
      }
    }).catch(err => {

    })
  }


  fetchAllFineList() {
    this.payrollService.getAllFineList({}).then((res: any) => {
      this.fineDetail = res.data
      this.fines = res.data
    }).catch(err => {

    })
  }

  fetchAllUsers() {
    this.sharedServices.getUsersForSelect({ all: true }).then((res: any) => {
      let { code, message, data } = res
      this.users = data.map(x => {
        return {
          id: x.id,
          name: x.first_name + ' ' + x.last_name
        }
      })
    })
  }

  open(content, id = null) {
    debugger
    // this.allowanceForm.reset()
    this.fineForm.reset()
    if (id) {

      this.fetchFineById(id)
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  addFineForUser() {
    this.payrollService.addFine(this.fineForm.value).then((res: any) => {
      debugger
      let { code, message } = res
      if (code == 200) {
        this.toaster.success(message)
        this.close()
        this.fetchAllFineList()

      }

    }).catch(err => {

    })
  }


  setForm() {
    debugger
    this.fineForm.get("user_id").setValue(this.fines.user_id)
    this.fineForm.get("type_id").setValue(this.fines.type_id)
    this.fineForm.get("amount").setValue(this.fines.amount)
    this.fineForm.get("description").setValue(this.fines.description)
   this.fineForm.get("month").setValue(this.fines.year+'-'+this.fines.month)
    this.fineForm.get("id").setValue(this.fines.id)
  }


  fetchFineById(id) {
    this.payrollService.getFineById(id).then((res: any) => {
        if (id) {
          let { code, message, data } = res
          if (code == 200) {
            this.fines = data
            this.setForm()
          }
        }
      })
    }
    delete(id){
      this.payrollService.deleteFineById(id).then((res :any)=>{
        debugger
        let {code , message } = res
        if(code == 200) {
          this.toaster.success(message)
          this.fetchAllFineList()
        }
      })
    }

    getMonthYear(month, year) {
      let date = `${year}-${month}`
  
      return moment(date,'YYYY-MM').format("MMMM YYYY")
    }
}
