import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BonusesComponent } from './bonuses/bonuses.component';
import { FineComponent } from './fine/fine.component';
import { DeductionComponent } from './deduction/deduction.component';
import { LoanComponent } from './loan/loan.component';
import { SalariesComponent } from './salaries/salaries.component';
import { PayslipComponent } from './payslip/payslip.component';
import { PayrollSheetComponent } from './payroll-sheet/payroll-sheet.component';

const routes: Routes = [
  {
    path: "",
    children: [{
      path: "bonuses",
      component: BonusesComponent
    },
    {
      path: "fine",
      component: FineComponent
    },
    {
      path: 'deduction',
      component: DeductionComponent
    },
  {
    path: 'loan',
    component:LoanComponent
  },
  {
    path: 'payslip',
    component:PayslipComponent
  },
  {
    path: 'sheet',
    component:PayrollSheetComponent
  },
{
  path : 'salaries',
  component:SalariesComponent
}]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayrollManagementRoutingModule { }
