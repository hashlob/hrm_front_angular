import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayrollManagementRoutingModule } from './payroll-management-routing.module';
import { BonusesComponent } from './bonuses/bonuses.component';
import { NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';
import { FineComponent } from './fine/fine.component';
import { DeductionComponent } from './deduction/deduction.component';
import { LoanComponent } from './loan/loan.component';
import { SalariesComponent } from './salaries/salaries.component';
import { PayslipComponent } from './payslip/payslip.component';
import { PayrollSheetComponent } from './payroll-sheet/payroll-sheet.component';
import { ListSalariesComponent } from './salaries/list-salaries/list-salaries.component';
import { MarkIncrementComponent } from './salaries/mark-increment/mark-increment.component';

@NgModule({
  declarations: [BonusesComponent, FineComponent, DeductionComponent, LoanComponent, SalariesComponent, PayslipComponent, PayrollSheetComponent,ListSalariesComponent, MarkIncrementComponent],
  imports: [
    CommonModule,
    PayrollManagementRoutingModule,
    NgbDropdownModule,
    FormsModule,
    NgbTabsetModule,
    
    ReactiveFormsModule,
    NgxDatatableModule,
    NgSelectModule
  ]
})
export class PayrollManagementModule { }
