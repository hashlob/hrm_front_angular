import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PayrollService } from '../payroll.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payroll-sheet',
  templateUrl: './payroll-sheet.component.html',
  styleUrls: ['./payroll-sheet.component.scss']
})
export class PayrollSheetComponent implements OnInit {

  @ViewChild('pdfarea') pdfTable: ElementRef;
  month
  year
  selectedMonth = ''
  columns = [
    { name: 'No' },
    { name: 'Employee' },
    { name: 'Salary' },
    { name: 'Bonuses' },
    { name: 'Fines' },
    { name: 'Deductions' },
    { name: 'Allowances' },
    { name: 'Loans' },
    { name: 'Net Salary' },
    // { name: 'Actions' },

  ];

  payrollSheet = []

  constructor(
    private payrollService: PayrollService,
    private toaster: ToastrService
  ) { }

  ngOnInit() {
    let date = new Date;
    this.setMonth(date);
    this.generatePayroll()
  }

  monthChange(e) {
    this.setMonth(e.target.value)
  }

  setMonth(date) {
    this.month = moment(date).format("MM")
    this.year = moment(date).format("YYYY")
    this.selectedMonth = moment(date).format("YYYY-MM")
  }

  generatePayroll() {
    let params = {
      month: this.month,
      year: this.year
    }
    this.payrollService.generatePayroll(params)
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.payrollSheet = data
        }
      }).catch((err: any) => {
        let { status, error } = err

      })
  }

  savePayroll() {
    let payload = {
      month: this.month,
      year: this.year,
      payroll_details: this.payrollSheet
    }

    this.payrollService.savePayroll(payload)
      .then((res: any) => {
        let { code, data, message } = res

        if (code == 200) {
          this.toaster.success(message)
          this.generatePayroll()
        }
      }).catch((err: any) => {
        let { status, error } = err
        this.toaster.info(error.message)
      })
  }

  lockPayroll() {
    let payload = {
      month: this.month,
      year: this.year,
    }
    this.payrollService.lockPayroll(payload)
      .then((res: any) => {
        let { code, data, message } = res
        if (code == 200) {
          this.toaster.success(message)
        }
      }).catch((err: any) => {
        let { status, error } = err
        this.toaster.info(error.message)
      })
  }

  downloadPdf() {
    //   const doc = new jsPDF('p', 'mm', 'a4');

    //   const specialElementHandlers = {
    //     '#editor': function (element, renderer) {
    //       return true;
    //     }
    //   };

    //   const pdfTable = this.pdfTable.nativeElement;

    //   doc.fromHTML(pdfTable, 15, 15, {
    //     width: 300,
    //     elementHandlers: specialElementHandlers
    // });

    //   doc.save('tableToPdf.pdf');

    var data = document.getElementById('pdfarea');  //Id of the table
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      let imgWidth = 208;
      let pageHeight = 295;
      let imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      let position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('MYPdf.pdf'); // Generated PDF   
    });
  }

  // print() {
  //   let printContents, popupWin;
  //   printContents = document.getElementById('print-section').innerHTML;
  //   popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
  //   popupWin.document.open();
  //   popupWin.document.write(`
  //     <html>
  //       <head>
  //         <title>Print tab</title>
  //         <style>
  //         //........Customized style.......
  //         </style>
  //       </head>
  //   <body onload="window.print();window.close()">${printContents}</body>
  //     </html>`
  //   );
  //   popupWin.document.close();
  // }

}
