import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import AppConfig from 'app/shared/app-config/appConfig';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PayrollService } from '../payroll.service';
import * as moment from "moment"

@Component({
  selector: 'app-bonuses',
  templateUrl: './bonuses.component.html',
  styleUrls: ['./bonuses.component.scss']
})
export class BonusesComponent implements OnInit {
  columns = [
    { name: 'No' },
    { name: 'Amount' },
    { name: 'Bonus Type' },
    { name: 'Name' },
    { name: 'Month' },
    { name: 'Description' },
    { name: 'Actions' },

  ];
  row = [];
  bonuseDetail = []
  bonusType = []
  users = []
  bonuses = null
  constructor(private sharedServices: SharedService,
    private fb: FormBuilder,
    private router: Router,
    private toaster: ToastrService,
    private payrollService: PayrollService,
    private modalService: NgbModal, ) { }

  ngOnInit() {

    this.fetchBonusType()
    this.fetchAllUsers()
    this.fetchAllBonusList()
  }
  bonusForm = this.fb.group({
    user_id: ['', Validators.required],
    type_id: ['', Validators.required],
    amount: ['', Validators.required],
    description: ['', Validators.required],
    month: ['', Validators.required],
    id: ''
  })

  fetchBonusType() {
    this.sharedServices.getDBVariable({ type_id: AppConfig.DBVariablesId.BonusType }).then((res: any) => {
      let { code, message, data } = res
      if (code == 200) {
        this.bonusType = data[0].details.map(x => {
          return {
            id: x.id,
            name: x.options
          }
        })
      }
    }).catch(err => {

    })
  }


  fetchAllBonusList() {
    this.payrollService.getAllBousesList({}).then((res: any) => {
      this.bonuseDetail = res.data
      this.bonuses = res.data
    }).catch(err => {

    })
  }
  fetchAllUsers() {
    this.sharedServices.getUsersForSelect({ all: true }).then((res: any) => {
      let { code, message, data } = res
      this.users = data.map(x => {
        return {
          id: x.id,
          name: x.first_name + ' ' + x.last_name
        }
      })
    })
  }


  open(content, id = null) {
    debugger
    // this.allowanceForm.reset()
    this.bonusForm.reset()
    if (id) {

      this.fetchBonusById(id)
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  addBonusForUser() {
    this.payrollService.addBonus(this.bonusForm.value).then((res: any) => {
      debugger
      let { code, message } = res
      if (code == 200) {
        this.toaster.success(message)
        this.close()
        this.fetchAllBonusList()

      }

    }).catch(err => {

    })
  }

  setForm() {
    debugger
    this.bonusForm.get("user_id").setValue(this.bonuses.user_id)
    this.bonusForm.get("type_id").setValue(this.bonuses.type_id)
    this.bonusForm.get("amount").setValue(this.bonuses.amount)
    this.bonusForm.get("description").setValue(this.bonuses.description)
    this.bonusForm.get("month").setValue(this.bonuses.year + '-' + this.bonuses.month)
    this.bonusForm.get("id").setValue(this.bonuses.id)
  }

  fetchBonusById(id) {
    this.payrollService.getBonusById(id).then((res: any) => {
      if (id) {
        let { code, message, data } = res
        if (code == 200) {
          this.bonuses = data
          this.setForm()
        }
      }
    })
  }

  delete(id) {
    this.payrollService.deleteBonusById(id).then((res: any) => {
      debugger
      let { code, message } = res
      if (code == 200) {
        this.toaster.success(message)
        this.fetchAllBonusList()
      }
    })
  }

  getMonthYear(month, year) {
    let date = `${year}-${month}`

    return moment(date,'YYYY-MM').format("MMMM YYYY")
  }
}
