import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PayrollService } from '../payroll.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import AppConfig from 'app/shared/app-config/appConfig';
import * as moment from "moment"

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.scss']
})
export class LoanComponent implements OnInit {

  constructor(private sharedServices: SharedService,
    private fb: FormBuilder,
    private router: Router,
    private toaster: ToastrService,
    private payrollService: PayrollService,
    private modalService: NgbModal, ) { }
  columns = [
    { name: 'No' },
    { name: 'Amount' },
    { name: 'Loan Type' },
    { name: 'Name' },
    { name: 'Started From' },
    { name: 'No of installments' },
    { name: 'Installments left' },
    { name: 'Actions' },

  ];
  row = [];
  loanDetail = []
  loanType = []
  users = []
  loans = null
  ngOnInit() {
    this.fetchLoanType()
    this.fetchAllUsers()
    this.fetchAllLoanList()
  }
  loanForm = this.fb.group({
    user_id: ['', Validators.required],
    type_id: ['', Validators.required],
    amount: ['', Validators.required],
    description: ['', Validators.required],
    start_month: ['',Validators.required],
    installments:['',Validators.required],
    id: ''
  })
  fetchLoanType() {
    this.sharedServices.getDBVariable({ type_id: AppConfig.DBVariablesId.LoanType }).then((res: any) => {
      let { code, message, data } = res
      if (code == 200) {
        this.loanType = data[0].details.map(x => {
          return {
            id: x.id,
            name: x.options
          }
        })
      }
    }).catch(err => {

    })
  }



  fetchAllUsers() {
    this.sharedServices.getUsersForSelect({ all: true }).then((res: any) => {
      let { code, message, data } = res
      this.users = data.map(x => {
        return {
          id: x.id,
          name: x.first_name + ' ' + x.last_name
        }
      })
    })
  }


  open(content, id = null) {
    debugger
    // this.allowanceForm.reset()
    this.loanForm.reset()
    if (id) {

      this.fetchFineById(id)
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }
  addLoanForUser() {
    this.payrollService.addLoan(this.loanForm.value).then((res: any) => {
      debugger
      let { code, message } = res
      if (code == 200) {
        this.toaster.success(message)
        this.close()
        this.fetchAllLoanList()

      }

    }).catch(err => {

    })
  }

  fetchAllLoanList() {
    this.payrollService.getAllLoansList({}).then((res: any) => {
      debugger
      this.loanDetail = res.data
      this.loans = res.data
    }).catch(err => {

    })
  }
  setForm() {
    debugger
    this.loanForm.get("user_id").setValue(this.loans.user_id)
    this.loanForm.get("type_id").setValue(this.loans.type_id)
    this.loanForm.get("amount").setValue(this.loans.amount)
    this.loanForm.get("description").setValue(this.loans.description)
    this.loanForm.get("installments").setValue(this.loans.installments)
   this.loanForm.get("start_month").setValue(this.loans.year+'-'+this.loans.start_month)
    this.loanForm.get("id").setValue(this.loans.id)
  }



  fetchFineById(id) {
    this.payrollService.getLoanById(id).then((res: any) => {
        if (id) {
          let { code, message, data } = res
          if (code == 200) {
            this.loans = data
            this.setForm()
          }
        }
      })
    }
    delete(id){
      this.payrollService.deleteLoanById(id).then((res :any)=>{
        debugger
        let {code , message } = res
        if(code == 200) {
          this.toaster.success(message)
          this.fetchAllLoanList()
        }
      })
    }

    getMonthYear(month, year) {
      let date = `${year}-${month}`
  
      return moment(date,'YYYY-MM').format("MMMM YYYY")
    }
}
