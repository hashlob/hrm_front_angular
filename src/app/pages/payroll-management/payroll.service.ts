import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import * as moment from "moment"

@Injectable({
  providedIn: 'root'
})
export class PayrollService {
  baseUrl = AppConfig.API_ENDPOINT + "/payroll";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  getPayslip(params) {
    return this.http.get(`${this.baseUrl}/getEmployeePayslip`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  generatePayroll(params) {
    return this.http.get(`${this.baseUrl}/generatePayroll`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  savePayroll(payload) {
    return this.http.post(`${this.baseUrl}/savePayroll`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  lockPayroll(payload) {
    return this.http.post(`${this.baseUrl}/lockPayroll`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  addBonus(payload) {

    if (payload) {
      debugger
      let year = moment(payload.month).format("YYYY")
      let month = moment(payload.month).format("MM")
      payload.month = month
      payload.year = year
      return this.http.post(`${this.baseUrl}/addBonus`, payload, {
        headers: this.headers
      }).toPromise()

    }

  }
  getAllBousesList(params) {
    return this.http.get(`${this.baseUrl}/getBonuses`, {
      headers: this.headers,
      params
    }).toPromise()
  }
  getBonusById(id) {
    return this.http.get(`${this.baseUrl}/getBonusById/${id}`, {
      headers: this.headers
    }).toPromise()
  }
  deleteBonusById(id) {
    return this.http.post(`${this.baseUrl}/removeBonus/${id}`, {}, {
      headers: this.headers
    }).toPromise()
  }






  // Fine
  addFine(payload) {

    if (payload) {
      debugger
      let year = moment(payload.month).format("YYYY")
      let month = moment(payload.month).format("MM")
      payload.month = month
      payload.year = year
      return this.http.post(`${this.baseUrl}/addFine`, payload, {
        headers: this.headers
      }).toPromise()

    }

  }
  getAllFineList(params) {
    return this.http.get(`${this.baseUrl}/getFines`, {
      headers: this.headers,
      params
    }).toPromise()
  }
  getFineById(id) {
    return this.http.get(`${this.baseUrl}/getFineById/${id}`, {
      headers: this.headers
    }).toPromise()
  }
  deleteFineById(id) {
    return this.http.post(`${this.baseUrl}/removeFine/${id}`, {}, {
      headers: this.headers
    }).toPromise()
  }







  // deduction


  addDeduction(payload) {

    if (payload) {
      debugger
      let year = moment(payload.month).format("YYYY")
      let month = moment(payload.month).format("MM")
      payload.month = month
      payload.year = year
      return this.http.post(`${this.baseUrl}/addDeduction`, payload, {
        headers: this.headers
      }).toPromise()

    }

  }
  getAllDeductionList(params) {
    return this.http.get(`${this.baseUrl}/getDeductions`, {
      headers: this.headers,
      params
    }).toPromise()
  }
  getDeductionById(id) {
    return this.http.get(`${this.baseUrl}/getDeductionById/${id}`, {
      headers: this.headers
    }).toPromise()
  }
  deleteDeductionById(id) {
    return this.http.post(`${this.baseUrl}/removeDeduction/${id}`, {}, {
      headers: this.headers
    }).toPromise()
  }




  // loan

  addLoan(payload) {

    if (payload) {
      debugger
      let year = moment(payload.start_month).format("YYYY")
      let start_month = moment(payload.start_month).format("MM")
      payload.start_month = start_month
      payload.year = year
      return this.http.post(`${this.baseUrl}/addLoan`, payload, {
        headers: this.headers
      }).toPromise()

    }

  }
  getAllLoansList(params) {
    return this.http.get(`${this.baseUrl}/getLoans`, {
      headers: this.headers,
      params
    }).toPromise()
  }
  getLoanById(id) {
    return this.http.get(`${this.baseUrl}/getLoanById/${id}`, {
      headers: this.headers
    }).toPromise()
  }
  deleteLoanById(id) {
    return this.http.post(`${this.baseUrl}/removeLoan/${id}`, {}, {
      headers: this.headers
    }).toPromise()
  }






  // Salaries

  getAllEmployeeSalaries() {
    return this.http.get(`${this.baseUrl}/getAllSalaries`, {
      headers: this.headers
    }
    ).toPromise()
  }

  addIncreement(payload) {
    return this.http.post(`${this.baseUrl}/markSalaryIncreament`, payload, {
      headers: this.headers
    }
    ).toPromise()

  }
}
