import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/shared.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ZoneService } from '../zone.service';

@Component({
  selector: 'app-list-zone',
  templateUrl: './list-zone.component.html',
  styleUrls: ['./list-zone.component.scss']
})
export class ListZoneComponent implements OnInit {

  columns = [
    { name: 'No' },
    { name: 'Name' },
    { name: 'Manager' },
    { name: 'Actions' }

  ];

  zones = []
  managers = []
  zoneDetail = null
  id = null

  constructor(
    private sharedService: SharedService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toaster: ToastrService,
    private zoneService: ZoneService
  ) { }
  zoneForm = this.fb.group({
    manager_id: [],
    name: [],
    id: []
  })
  ngOnInit() {
    this.fetchZones()
    this.fetchManagers();
  }

  fetchZones() {
    this.zoneService.getZones({})
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.zones = data
        }
      }).catch(err => console.log(err))
  }

  fetchManagers() {
    this.sharedService.getUsersForSelect({ userType: 3 })
      .then((res: any) => {
        let { data, code, message } = res
        if (code == 200) {
          this.managers = data.map(x => {
            return {
              id: x.id,
              name: x.user.first_name + " " + x.user.last_name
            }
          })
        }
      }).catch(err => {
        let { status, error = null } = err
        console.log(err)
      })
  }
  open(content, id = null) {
    this.zoneForm.reset()
    if (id) {
      this.id = id
      this.fetchZoneById(id)
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  fetchZoneById(id) {
    this.zoneService.fetchZoneById(id)
      .then((res: any) => {
        let { data, message, code } = res
        this.zoneDetail = data
        this.setForm()

      })
  }

  setForm() {
    this.zoneForm.get("manager_id").setValue(this.zoneDetail.manager_id)
    this.zoneForm.get("name").setValue(this.zoneDetail.name)
    this.zoneForm.get("id").setValue(this.id)
  }

  close() {
    this.modalService.dismissAll();
  }

  addZone() {
    this.zoneService.addZone(this.zoneForm.value)
      .then((res: any) => {
        let { code, message, data } = res
        this.close()
        this.toaster.success(message)
        this.fetchZones()
        this.id = null
        this.zoneForm.reset()
      }).catch(err => console.log(err))
  }

}
