import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListZoneComponent } from './list-zone/list-zone.component';

const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "list",
        component: ListZoneComponent,
        data: {
          title: "Manage Zone"
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ZoneRoutingModule { }
