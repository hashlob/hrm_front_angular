import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ZoneService {

  baseUrl = AppConfig.API_ENDPOINT + "/zone";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  addZone(payload) {
    return this.http.post(this.baseUrl, payload, {
      headers: this.headers
    }).toPromise()
  }

  getZones(params) {
    return this.http.get(this.baseUrl, {
      headers: this.headers,
      params
    }).toPromise()
  }

  fetchZoneById(id) {
    return this.http.get(`${this.baseUrl}/fetchZoneById/${id}`, {
      headers: this.headers,
    }).toPromise()
  }

  changeZoneStatus(payload) {
    return this.http.post(this.baseUrl, payload, {
      headers: this.headers,
    }).toPromise()
  }

}
