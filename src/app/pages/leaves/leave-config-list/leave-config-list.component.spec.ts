import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveConfigListComponent } from './leave-config-list.component';

describe('LeaveConfigListComponent', () => {
  let component: LeaveConfigListComponent;
  let fixture: ComponentFixture<LeaveConfigListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveConfigListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveConfigListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
