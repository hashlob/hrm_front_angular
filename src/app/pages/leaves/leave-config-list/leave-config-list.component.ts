import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { SharedService } from 'app/shared/shared.service';
import AppConfig from 'app/shared/app-config/appConfig';
import { LeavesService } from '../leaves.service';
import { LeaveConfigService } from '../leave-config.service';

@Component({
  selector: 'app-leave-config-list',
  templateUrl: './leave-config-list.component.html',
  styleUrls: ['./leave-config-list.component.scss']
})
export class LeaveConfigListComponent implements OnInit {

  constructor(
    private toaster: ToastrService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private leaveService: LeavesService,
    private leaveConfigService: LeaveConfigService,
    private sharedService: SharedService,
  ) { }


  columns = [
    { name: 'No' },
    { name: 'Leave Type' },
    { name: 'Quantity' },
    { name: 'Actions' },
  ];

  optionForm = this.fb.group({
    config_id: ['', [Validators.required]],
    leave_type: ['', [Validators.required]],
    quantity: ['', [Validators.required]],
    id: [null]

  })

  rows = []
  DBVariables = []
  leaves = []

  id

  ngOnInit() {
    this.fetchLeaveConfig()
    this.fetchLeaveTypes()
  }

  fetchLeaveTypes() {
    this.sharedService.getDBVariable({ type_id: AppConfig.DBVariablesId.leavesType })
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.leaves = data[0].details
        }
      }).catch(err => {
        console.log(err)
      })
  }

  fetchLeaveConfig() {
    this.sharedService.getDBVariable({ type_id: AppConfig.DBVariablesId.LeaveConfig })
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.DBVariables = data[0].details
        }
      }).catch(err => {
        console.log(err)
      })
  }

  open(content, id = null) {
    this.optionForm.reset()
    // this.optionForm.reset()
    // if (id) {
    //   this.id = id
    //   this.fetchOptionById(id)
    // }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: "lg" })
  }

  close() {
    this.modalService.dismissAll();
  }

  onChangeSelect(event) {
    this.rows = []
    this.fetchLeaveConfigDetails(event.target.value)
  }

  fetchLeaveConfigDetails(config_id) {
    this.leaveConfigService.getForConfig({ config_id })
      .then((res: any) => {
        let { code, message, data } = res

        if (code == 200) {
          this.rows = data
        }
      }).catch(err => {
        console.log(err)
      })
  }

  addOptionDetails() {


    this.leaveConfigService.createLeaveConfig(this.optionForm.value)
      .then((res: any) => {
        let { code, message, data } = res
        this.close()
        this.fetchLeaveConfigDetails(this.optionForm.value.config_id)
        this.toaster.success(message)
        this.id = null
        this.optionForm.reset()

      }).catch(err => {
        console.log(err)
      })
  }

  delete(id) {
    this.leaveConfigService.delete(id)
      .then((res: any) => {
        let { code, message, data } = res
        this.toaster.success(message)
        let finalData = this.rows.filter(x => x.id != id)
        this.rows = finalData
      }).catch(err => {
        console.log(err)
      })
  }
}
