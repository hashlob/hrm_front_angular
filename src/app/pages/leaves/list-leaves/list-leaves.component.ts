import { Component, OnInit } from '@angular/core';
import { LeavesService } from '../leaves.service';
import { SharedService } from 'app/shared/shared.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import AppConfig from 'app/shared/app-config/appConfig';

@Component({
  selector: 'app-list-leaves',
  templateUrl: './list-leaves.component.html',
  styleUrls: ['./list-leaves.component.scss']
})
export class ListLeavesComponent implements OnInit {

  constructor(
    private leavesServie: LeavesService,
    private sharedServices: SharedService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toaster: ToastrService,
    private router: Router
  ) { }
  processLeaveForm = this.fb.group({
    status_message: ['', [Validators.required]]
  })

  rows = [];
  columns = [
    { name: 'No' },
    { name: 'Applied By' },
    { name: 'From' },
    { name: 'To' },
    { name: 'Department' },
    { name: 'Status' },
    { name: 'Actions' }
  ];
  isRejected = false

  loading = true
  userId = null
  departmentId = null

  row = null

  count = 0

  ngOnInit() {
    this.userId = AppConfig.getUserId()
    this.departmentId = AppConfig.getDepartmentId()
    this.fetchLeaveRequests()
  }

  fetchLeaveRequests() {
    if (!this.departmentId) {
      this.toaster.info("Department not assigned.")
      return
    }
    let params = {
      user_id: this.userId,
      department_id: this.departmentId
    }
    this.leavesServie.getLeaveRequests(params)
      .then((res: any) => {
        this.loading = false
        let { code, message, data } = res

        if (code == 200) {
          this.rows = data
        }
      }).catch(err => {
        this.loading = false
        console.log(err)
      })
  }

  processLeave(row) {

    let status = this.isRejected ? "0" : "1"
    let formData = new FormData
    formData.append('department_id', this.departmentId)
    formData.append('manager_id', this.userId)
    formData.append('approved_status', status)
    formData.append('leave_id', row.id)

    if (this.isRejected) {
      formData.append('status_message', this.processLeaveForm.value.status_message)
    }

    this.leavesServie.processLeave(formData)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.toaster.success(message)
          this.fetchLeaveRequests();
          this.processLeaveForm.reset();
        }
      }).catch(err => {
        console.log(err)
      })

  }

  open(content, row) {
    this.row = row
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
  }

}
