import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LeavesService {

  baseUrl = AppConfig.API_ENDPOINT + "/leaveRequest";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  getLeaveRequests(params) {
    return this.http.get(`${this.baseUrl}/getLeavesDetails`, {
      headers: this.headers,
      params: params
    }).toPromise()
  }

  applyLeave(payload) {
    return this.http.post(`${this.baseUrl}/apply`, payload, {
      headers: this.headers
    }).toPromise()
  }

  processLeave(payload) {
    return this.http.post(`${this.baseUrl}/processLeave`, payload, {
      headers: this.headers
    })
      .toPromise()
  }

 
}
