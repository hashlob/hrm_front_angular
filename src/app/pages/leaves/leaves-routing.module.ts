import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListLeavesComponent } from './list-leaves/list-leaves.component';
import { ApplyLeaveComponent } from './apply-leave/apply-leave.component';
import { LeaveConfigListComponent } from './leave-config-list/leave-config-list.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'requests',
        component: ListLeavesComponent,
        data: {
          title: 'Leaves Requests'
        }
      },
      {
        path: 'apply',
        component: ApplyLeaveComponent,
        data:{
          title: 'Apply for Leave'
        }
      },
      {
        path: 'leave-config',
        component: LeaveConfigListComponent,
        data:{
          title: 'Leave Config'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeavesRoutingModule { }
