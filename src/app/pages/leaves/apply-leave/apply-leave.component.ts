import { Component, OnInit } from '@angular/core';
import { LeavesService } from '../leaves.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from 'app/shared/shared.service';
import AppConfig from '../../../shared/app-config/appConfig';

@Component({
  selector: 'app-apply-leave',
  templateUrl: './apply-leave.component.html',
  styleUrls: ['./apply-leave.component.scss']
})
export class ApplyLeaveComponent implements OnInit {

  constructor(
    private leavesServices: LeavesService,
    private sharedServices: SharedService,
    private fb: FormBuilder,
    private router: Router,
    private toaster: ToastrService
  ) { }

  leaveForm = this.fb.group({
    from: ['', [Validators.required]],
    to: ['', [Validators.required]],
    reason: ['', [Validators.required]],
    leave_type: ['', [Validators.required]]
  })
  leaveTypes = []
  userId = null
  departmentId = null

  ngOnInit() {

    this.fetchLeaveTypes();
    this.userId = AppConfig.getUserId()
    this.departmentId = AppConfig.getDepartmentId()
  }

  fetchLeaveTypes() {
    this.sharedServices.getDBVariable({ type_id: AppConfig.DBVariablesId.leavesType })
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.leaveTypes = data[0].details
        }
      }).catch(err => {
        console.log(err)
      })
  }

  applyForLeave() {
    let formData = new FormData
    this.leaveForm.value
    formData.append('leave_type', this.leaveForm.value.leave_type)
    formData.append('from', this.leaveForm.value.from)
    formData.append('to', this.leaveForm.value.to)
    formData.append('reason', this.leaveForm.value.reason)
    formData.append('applied_by', this.userId)
    formData.append('department_id', this.departmentId)

    this.leavesServices.applyLeave(formData)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.toaster.success(message)
          this.router.navigate(["/leaves/requests"])
        }
      }).catch(err => {
        let { status, error } = err
        if (status == 422) {
          this.toaster.info(error.message)
        }
        console.log(err)
      })
  }

}
