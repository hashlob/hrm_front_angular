import { Injectable } from '@angular/core';
import AppConfig from 'app/shared/app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LeaveConfigService {
  baseUrl = AppConfig.API_ENDPOINT + "/leaveConfiguration";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
  }

  getForConfig(params) {
    return this.http.get(`${this.baseUrl}/getForConfig`, {
      headers: this.headers,
      params
    })
      .toPromise()
  }

  createLeaveConfig(payload) {
    return this.http.post(`${this.baseUrl}`, payload, {
      headers: this.headers,
    })
      .toPromise()
  }

  delete(id) {
    return this.http.get(`${this.baseUrl}/delete/${id}`, {
      headers: this.headers,
    })
      .toPromise()
  }
}
