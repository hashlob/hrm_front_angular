import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeavesRoutingModule } from './leaves-routing.module';
import { ListLeavesComponent } from './list-leaves/list-leaves.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ApplyLeaveComponent } from './apply-leave/apply-leave.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { LeaveConfigListComponent } from './leave-config-list/leave-config-list.component';

@NgModule({
  declarations: [ListLeavesComponent, ApplyLeaveComponent, LeaveConfigListComponent],
  imports: [
    CommonModule,
    LeavesRoutingModule,
    NgxDatatableModule,
    FormsModule,
    NgbModalModule,
    NgbDropdownModule,
    ReactiveFormsModule
  ]
})
export class LeavesModule { }
