import { TestBed } from '@angular/core/testing';

import { LeaveConfigService } from './leave-config.service';

describe('LeaveConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LeaveConfigService = TestBed.get(LeaveConfigService);
    expect(service).toBeTruthy();
  });
});
