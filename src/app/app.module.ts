
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from "./shared/shared.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import {
  PerfectScrollbarModule,
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface
} from 'ngx-perfect-scrollbar';

import { AppComponent } from './app.component';
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";
import { FullLayoutComponent } from "./layouts/full/full-layout.component";

import { AuthService } from './shared/auth/auth.service';
import { AuthGuard } from './shared/auth/auth-guard.service';
import { RoleModule } from './pages/role/role.module';
import { ToastrModule } from 'ngx-toastr';
import { DepartmentModule } from './pages/department/department.module';
import { UserModule } from './pages/user/user.module';
import { UnitModule } from './pages/unit/unit.module';
import { DesignationModule } from './pages/designation/designation.module';
import { LeavesModule } from './pages/leaves/leaves.module';
import { SystemVariableModule } from './pages/system-variable/system-variable.module';
import { AttendenceModule } from './pages/attendence/attendence.module';
import { RequestLineModule } from './pages/request-line/request-line.module';
import { AllowanceModule } from './pages/allowance/allowance.module';
import { ZoneModule } from './pages/zone/zone.module';
import { TransferManagementModule } from './pages/transfer-management/transfer-management.module';
import { NotificationModule } from './pages/notification/notification.module';
import { PermissionModule } from './pages/permission/permission.module';
import { HiringManagementModule } from './pages/hiring-management/hiring-management.module';
import { PayrollManagementModule } from './pages/payroll-management/payroll-management.module';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelPropagation: false
};

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}


@NgModule({
  declarations: [AppComponent, FullLayoutComponent, ContentLayoutComponent],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    NgbModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    PerfectScrollbarModule,
    RoleModule,
    DepartmentModule,
    UserModule,
    UnitModule,
    DesignationModule,
    LeavesModule,
    SystemVariableModule,
    AttendenceModule,
    RequestLineModule,
    AllowanceModule,
    ZoneModule,
    TransferManagementModule,
    NotificationModule,
    PermissionModule,
    HiringManagementModule,
    PayrollManagementModule,
  ],
  providers: [
    AuthService,
    AuthGuard,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
