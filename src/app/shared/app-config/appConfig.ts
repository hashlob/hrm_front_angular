export default class AppConfig {
    public static API_ENDPOINT = "http://localhost:8000/api"
    // public static API_ENDPOINT = "http://119.159.242.5/hrm/public/api"

    public static DBVariablesId = {
        leavesType: 1,
        PaymentType: 2,
        Unit: 3,
        RequestLineType: 4,
        LeaveConfig: 5,
        Allowance: 6,
        BonusType: 7,
        DeductionType: 8,
        FineType: 9,
        LoanType: 10,
        DocumentType: 11,
        EmployeeType: 12,
    }


    public static getRoleId() {
        let userSystemDetail = localStorage.getItem('userSystemDetail')
        return JSON.parse(userSystemDetail).role_id
    }

    public static getPermissions() {
        let permissions = localStorage.getItem('permissions')
        return JSON.parse(permissions)
    }

    public static getUserId() {
        let userSystemDetail = localStorage.getItem('userSystemDetail')
        return JSON.parse(userSystemDetail).user_id
    }

    public static getDepartmentId() {
        let userDetail: any = localStorage.getItem('user')
        userDetail = JSON.parse(userDetail)
        if (userDetail.employment_details) {
            return userDetail.employment_details.department_id
        }
        return null
    }

    public static getUnitId() {
        let userSystemDetail: any = localStorage.getItem('userSystemDetail')
        userSystemDetail = JSON.parse(userSystemDetail)
        if (userSystemDetail.employment_details) {
            return userSystemDetail.employment_details.unit_id
        }
        return null
    }


}