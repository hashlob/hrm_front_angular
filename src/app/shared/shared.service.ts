import { Injectable } from '@angular/core';
import AppConfig from './app-config/appConfig';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  baseUrl = AppConfig.API_ENDPOINT + "/shared";
  token = localStorage.getItem("token")
  headers = new HttpHeaders();
  userObj = null
  userSystemDetail = null
  constructor(private http: HttpClient) {
    this.headers = this.headers.append("Authorization", `Bearer ${this.token}`)
    this.headers = this.headers.append("Accept", "application/json")
    this.userObj = localStorage.getItem('user')
    this.userSystemDetail = localStorage.getItem('userSystemDetail')

  }



  department_Id = null
  unit_Id = null


  public get userId(): string {
    return JSON.parse(this.userObj).id
  }

  roleId() {
    return JSON.parse(this.userSystemDetail).role_id
  }


  public set departmentId(value) {
    if (value) {
      this.department_Id = value
    } else {
      this.department_Id = JSON.parse(this.userObj).employment_details.department_id;
    }
  }

  public set unitId(value) {
    if (value) {
      this.unit_Id = value
    } else {
      this.unit_Id = JSON.parse(this.userObj).employment_details.unit_id;
    }
  }

  public get departmentId(): string {
    return this.department_Id = JSON.parse(this.userObj).employment_details.department_id;
  }


  public get unitId(): string {
    return this.unit_Id;
  }


  getUsersForSelect(params) {
    return this.http.get(`${this.baseUrl}/getUsersForSelect`, {
      headers: this.headers,
      params: params
    }).toPromise()
  }

  getRolesForSelect(params) {
    return this.http.get(`${this.baseUrl}/getRolesForSelect`, {
      headers: this.headers,
      params: params
    }).toPromise()
  }

  getUnitForSelect(params) {
    return this.http.get(`${this.baseUrl}/getUnitForSelect`, {
      headers: this.headers,
      params: params
    }).toPromise()
  }

  getDepartmentForSelect(params) {
    return this.http.get(`${this.baseUrl}/getDepartmentForSelect`, {
      headers: this.headers,
      params: params
    }).toPromise()
  }

  getDesignationForSelect(params) {
    return this.http.get(`${this.baseUrl}/getDesignationForSelect`, {
      headers: this.headers,
      params: params
    }).toPromise()
  }

  getDBVariable(params) {
    return this.http.get(`${this.baseUrl}/getDBVariable`, {
      headers: this.headers,
      params: params
    }).toPromise()
  }

  getSystemVariable() {
    return this.http.get(`${this.baseUrl}/getSystemVariable`, {
      headers: this.headers,
    }).toPromise()
  }

  createSytemVarable(payload) {
    return this.http.post(`${this.baseUrl}/createSytemVarable`, payload, {
      headers: this.headers,
    }).toPromise()
  }

  fetchOptionById(params) {
    return this.http.get(`${this.baseUrl}/fetchOptionById`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  fetchLeaveConfigs() {
    return this.http.get(`${this.baseUrl}/leaveConfig`, {
      headers: this.headers,
    }).toPromise()
  }

  getNotifications(params) {
    return this.http.get(`${this.baseUrl}/getNotifications`, {
      headers: this.headers,
      params
    }).toPromise()
  }

  readNotifications(id) {
    return this.http.get(`${this.baseUrl}/readNotifications/${id}`, {
      headers: this.headers,
    }).toPromise()
  }

}
