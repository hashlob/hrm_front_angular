import { Component, Output, EventEmitter, OnInit, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { LayoutService } from '../services/layout.service';
import { ConfigService } from '../services/config.service';
import { SharedService } from '../shared.service';
import { Router } from '@angular/router';
import AppConfig from '../app-config/appConfig';

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"]
})
export class NavbarComponent implements OnInit, AfterViewInit {
  currentLang = "en";
  toggleClass = "ft-maximize";
  placement = "bottom-right";
  unRead = 0;
  notifications = []
  public isCollapsed = true;
  @Output()
  toggleHideSidebar = new EventEmitter<Object>();

  public config: any = {};
  userId = null

  constructor(public translate: TranslateService, private layoutService: LayoutService, private configService: ConfigService,
    private sharedService: SharedService,
    private router: Router
  ) {
    const browserLang: string = translate.getBrowserLang();
    translate.use(browserLang.match(/en|es|pt|de/) ? browserLang : "en");

  }

  ngOnInit() {
    this.userId = AppConfig.getUserId()
    this.config = this.configService.templateConf;
    this.fetchNotifiications()
  }

  fetchNotifiications() {
    let params = {
      user_id: this.userId
    }
    this.sharedService.getNotifications(params)
      .then((res: any) => {
        let { code, message, data } = res
        if (code == 200) {
          this.notifications = data
          data.forEach((x) => {
            if (x.is_read == 0) {
              this.unRead = this.unRead + 1
            }
          });
        }
      }).catch(err => console.log(err))
  }

  navigateToProfile() {
    this.router.navigate([`/user/profile/${this.userId}`])
  }

  readNotification(id, path) {
    this.sharedService.readNotifications(id)
      .then((res: any) => {
        this.unRead = this.unRead - 1
        this.router.navigate([path])
      }).catch(err => console.log(err))
  }

  ngAfterViewInit() {
    if (this.config.layout.dir) {
      const dir = this.config.layout.dir;
      if (dir === "rtl") {
        this.placement = "bottom-left";
      } else if (dir === "ltr") {
        this.placement = "bottom-right";
      }
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/'])
  }

  ChangeLanguage(language: string) {
    this.translate.use(language);
  }

  ToggleClass() {
    if (this.toggleClass === "ft-maximize") {
      this.toggleClass = "ft-minimize";
    } else {
      this.toggleClass = "ft-maximize";
    }
  }

  toggleNotificationSidebar() {
    this.layoutService.emitChange(true);
  }

  toggleSidebar() {
    const appSidebar = document.getElementsByClassName("app-sidebar")[0];
    if (appSidebar.classList.contains("hide-sidebar")) {
      this.toggleHideSidebar.emit(false);
    } else {
      this.toggleHideSidebar.emit(true);
    }
  }
}
