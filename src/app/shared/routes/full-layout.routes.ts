import { Routes, RouterModule } from '@angular/router';

//Route for content layout with sidebar, navbar and footer
export const Full_ROUTES: Routes = [

  {
    path: 'full-layout',
    loadChildren: './pages/full-layout-page/full-pages.module#FullPagesModule'
  },
  {
    path: 'dashboard',
    loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'role',
    loadChildren: './pages/role/role.module#RoleModule'
  },
  {
    path: 'department',
    loadChildren: './pages/department/department.module#DepartmentModule'
  },
  {
    path: 'user',
    loadChildren: './pages/user/user.module#UserModule'
  },
  {
    path: "unit",
    loadChildren: './pages/unit/unit.module#UnitModule'

  },
  {
    path: "designation",
    loadChildren: './pages/designation/designation.module#DesignationModule'
  },
  {
    path: "leaves",
    loadChildren: './pages/leaves/leaves.module#LeavesModule'
  },
  {
    path: "sytemvariable",
    loadChildren: './pages/system-variable/system-variable.module#SystemVariableModule'
  },
  {
    path: "attendence",
    loadChildren: './pages/attendence/attendence.module#AttendenceModule'
  },
  {
    path: "requestLine",
    loadChildren: './pages/request-line/request-line.module#RequestLineModule'
  },
  {
    path: "allowances",
    loadChildren: './pages/allowance/allowance.module#AllowanceModule'
  },
  {
    path: "zone",
    loadChildren: './pages/zone/zone.module#ZoneModule'
  },
  {
    path: "tranfermanagement",
    loadChildren: './pages/transfer-management/transfer-management.module#TransferManagementModule'
  },
  {
    path: "notification",
    loadChildren: './pages/notification/notification.module#NotificationModule'
  },
  {
    path: "permissions",
    loadChildren: './pages/permission/permission.module#PermissionModule'
  },
  {
    path: "hiring",
    loadChildren: './pages/hiring-management/hiring-management.module#HiringManagementModule'
  },
  {
    path: "payroll",
    loadChildren: './pages/payroll-management/payroll-management.module#PayrollManagementModule'
  }

];